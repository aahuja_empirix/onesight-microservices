package com.discovery.controller;

import java.io.IOException;
import java.net.InetAddress;
import java.net.URI;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONObject;
//import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpEntity;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.discovery.entity.*; 

//import com.discovery.pojo.*;

@RestController
public class FindNetworkIps {

	@Autowired
	public RestTemplate restTemplate1;

	static Set<String> networkIps = new HashSet<String>();

	@RequestMapping(value = "/start", method = RequestMethod.GET)
	public String startScheduler() {

		ScheduledExecutorService executorService;
		executorService = Executors.newSingleThreadScheduledExecutor();
		executorService.scheduleAtFixedRate(() -> {
			try {
				try {
					run();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}, 0, 60, TimeUnit.MINUTES);

		return "Discovery Started";
	}

	public void run() throws IOException, Exception, InterruptedException {
		// TODO Auto-generated method stub

		boolean firstRun = false;
		if (networkIps.isEmpty())
			firstRun = true;

		System.out.println("Starting the thread, firstRun is " + firstRun);
		InetAddress localhost = InetAddress.getLocalHost();
		byte[] ip = localhost.getAddress();

		for (int i = 1; i <= 254; i++) {

			ip[3] = (byte) i;
			InetAddress address = InetAddress.getByAddress(ip);

			if (address.isReachable(100)) {
				String output = address.toString().substring(1);
				firstRun = false;
				if (firstRun) {

					System.out.println(output + " is on the network and added to the set");
					networkIps.add(output); // populate the set with all the
											// network IPs

				} else {
					// If this is not the first run check if this ip is already
					// present in set or not
					if (networkIps.contains(output)) {
						System.out.println(output + " is present in the set");
					} else {
						System.out.println(output + " is discovered");
						networkIps.add(output);

						Thread.sleep(2000);

						// http://localhost:9001/addSystemAPI

						SystemObject systemObject = new SystemObject();
						setSystemDetails(systemObject, output);

						URI uri = new URI("http://onesightapi-service/addSystemAPI");

						org.springframework.http.HttpHeaders headers = new org.springframework.http.HttpHeaders();
						headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
						HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

						String response = this.restTemplate1.postForEntity(uri, systemObject, String.class).getBody();

						JSONArray jsonArr = new JSONArray(response);
						
						for (int j = 0; j < jsonArr.length(); j++)
				        {
				            JSONObject jsonObj = jsonArr.getJSONObject(j);

				            System.out.println(jsonObj);
				            if(jsonObj.get("systemName").equals(output))
				            	systemObject.setId((Integer) jsonObj.get("id"));
				        }
						
						uri = new URI("http://onesightapi-service/discoverSystemAPI");
						this.restTemplate1.postForEntity(uri, systemObject, String.class);
						
						MonitorObject monitorObject = new MonitorObject();
				        setMonitorDetails(monitorObject, output, systemObject.getId());
				        
				        uri = new URI("http://onesightapi-service/addProfileMonitorAPI");
						this.restTemplate1.postForEntity(uri, monitorObject, String.class);
					}
				}

			}

		}
		System.out.println("No. of IP addresses discovered=" + networkIps.size());

	}

	private static void setSystemDetails(SystemObject systemObject, String systemName) {
		// TODO Auto-generated method stub
		systemObject.setUsername("admin");
		systemObject.setPassword("YWRtaW4=");
		systemObject.setOneSightDomain("http://10.90.1.3:8080");
		systemObject.setSystemName(systemName);
		systemObject.setSystemLevel("AdvancedServer");
		systemObject.setOsType("NT_OS");

		SystemDetails sysDetails = new SystemDetails();

		HashMap<String, String> systemTab = new HashMap<String, String>();
		systemTab.put("locationId", "1");
		systemTab.put("onFailure", "Run Traceroute");
		systemTab.put("criticalState", "");
		systemTab.put("goodState", "");
		systemTab.put("warningState", "");
		sysDetails.setSystemTab(systemTab);

		HashMap<String, String> snmpTab = new HashMap<String, String>();
		snmpTab.put("communityString", "public");
		snmpTab.put("timeout", "15");
		snmpTab.put("retry", "1");
		snmpTab.put("port", "161");
		snmpTab.put("snmpVersion", "Snmp_Version1");
		snmpTab.put("authMode", "NoAuth/NoPriv");
		snmpTab.put("username", "");
		snmpTab.put("password", "");
		snmpTab.put("authType", "MD5");
		snmpTab.put("privacyMode", "AES-128");
		snmpTab.put("privacyPassword", "");
		snmpTab.put("contextName", "");
		sysDetails.setSnmpTab(snmpTab);

		HashMap<String, String> snmpTrapTab = new HashMap<String, String>();
		snmpTrapTab.put("communityString", "public");
		snmpTrapTab.put("snmpV3Config", "");
		sysDetails.setSnmpTrapTab(snmpTrapTab);

		HashMap<String, String> virtualAgentTab = new HashMap<String, String>();
		virtualAgentTab.put("connectionMethod", "SSH");
		virtualAgentTab.put("port", "22");
		virtualAgentTab.put("prompt", "");
		virtualAgentTab.put("charset", "");
		virtualAgentTab.put("remoteOS", "Solaris-v5_8");
		virtualAgentTab.put("remoteName", "");
		virtualAgentTab.put("remotePassword", "");
		virtualAgentTab.put("remoteKeyFile", "");
		virtualAgentTab.put("remoteLoginTimeout", "30000");
		virtualAgentTab.put("remoteLineTimeout", "30000");
		virtualAgentTab.put("loginStateMachine", "-1");
		sysDetails.setVirtualAgentTab(virtualAgentTab);

		HashMap<String, String> jmxTab = new HashMap<String, String>();
		jmxTab.put("jmxAppName", "weblogic9.0");
		jmxTab.put("jmxAppNameOther", "");
		jmxTab.put("port", "7001");
		jmxTab.put("jmxSecureConnection", "false");
		jmxTab.put("jmxUsername", "");
		jmxTab.put("jmxPassword", "");
		jmxTab.put("jmxTrustFile", "");
		jmxTab.put("jmxTrustPassword", "");
		jmxTab.put("jmxKeyFile", "");
		jmxTab.put("jmxKeyPassword", "");
		sysDetails.setJmxTab(jmxTab);

		HashMap<String, String> windowsNtTab = new HashMap<String, String>();
		windowsNtTab.put("username", "Administrator");
		windowsNtTab.put("password", "123Empirix!");
		windowsNtTab.put("domain", "");
		sysDetails.setWindowsNtTab(windowsNtTab);

		systemObject.setDetails(sysDetails);
	}

	public static void setMonitorDetails(MonitorObject monitorObject, String system, Integer systemId) {

		monitorObject.setSystemName(system);
		monitorObject.setUsername("admin");
		monitorObject.setPassword("YWRtaW4=");
		monitorObject.setOneSightDomain("http://10.90.1.3:8080");
		monitorObject.setMonitorName("Microservices EndToEnd Windows Profile: " + system);
		monitorObject.setMonitorType("Microsoft Windows Monitor");
		monitorObject.setSelectedTreeId(6);
		monitorObject.setMonitorGroupId(41);
		monitorObject.setSystemId(systemId);
		monitorObject.setAltType("Standard Alert Message");
	}

}
