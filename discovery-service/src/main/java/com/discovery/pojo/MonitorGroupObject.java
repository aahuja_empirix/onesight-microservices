package com.discovery.pojo;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class MonitorGroupObject {

	private String username;
	private String password;
	private String monitorGroupName;
	private String oneSightDomain;
	private String monitorGroupDesc;
	private ArrayList<Integer> id;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMonitorGroupName() {
		return monitorGroupName;
	}

	public void setMonitorGroupName(String monitorGroupName) {
		this.monitorGroupName = monitorGroupName;
	}

	public String getOneSightDomain() {
		return oneSightDomain;
	}

	public void setOneSightDomain(String oneSightDomain) {
		this.oneSightDomain = oneSightDomain;
	}

	public String getMonitorGroupDesc() {
		return monitorGroupDesc;
	}

	public void setMonitorGroupDesc(String monitorGroupDesc) {
		this.monitorGroupDesc = monitorGroupDesc;
	}

	public ArrayList<Integer> getId() {
		return id;
	}

	public void setId(ArrayList<Integer> id) {
		this.id = id;
	}

}
