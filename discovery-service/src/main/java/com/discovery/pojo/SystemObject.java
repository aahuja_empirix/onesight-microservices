package com.discovery.pojo;

public class SystemObject {

	private String username;
	private String password;
	private String oneSightDomain;
	private Integer id;
	private String systemName;
	private String systemLevel;
	private String osType;
	private Boolean isDiscovered;
	private SystemDetails details;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getOneSightDomain() {
		return oneSightDomain;
	}

	public void setOneSightDomain(String oneSightDomain) {
		this.oneSightDomain = oneSightDomain;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSystemName() {
		return systemName;
	}

	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	public String getSystemLevel() {
		return systemLevel;
	}

	public void setSystemLevel(String systemLevel) {
		this.systemLevel = systemLevel;
	}

	public String getOsType() {
		return osType;
	}

	public void setOsType(String osType) {
		this.osType = osType;
	}

	public Boolean getIsDiscovered() {
		return isDiscovered;
	}

	public void setIsDiscovered(Boolean isDiscovered) {
		this.isDiscovered = isDiscovered;
	}

	public SystemDetails getDetails() {
		return details;
	}

	public void setDetails(SystemDetails details) {
		this.details = details;
	}

}
