package com.discovery.pojo;

public class DowntimeObject {

	private String username;
	private String password;
	private String scheduleName;
	private String oneSightDomain;
	private Integer id;
	private Integer planId;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getScheduleName() {
		return scheduleName;
	}

	public void setScheduleName(String scheduleName) {
		this.scheduleName = scheduleName;
	}

	public String getOneSightDomain() {
		return oneSightDomain;
	}

	public void setOneSightDomain(String oneSightDomain) {
		this.oneSightDomain = oneSightDomain;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPlanId() {
		return planId;
	}

	public void setPlanId(Integer planId) {
		this.planId = planId;
	}
}
