package com.discovery.pojo;

import java.util.ArrayList;

public class LocationObject {

	private String username;
	private String password;
	private String locationName;
	private String oneSightDomain;
	private int id;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getOneSightDomain() {
		return oneSightDomain;
	}

	public void setOneSightDomain(String oneSightDomain) {
		this.oneSightDomain = oneSightDomain;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
