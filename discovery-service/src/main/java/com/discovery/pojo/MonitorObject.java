package com.discovery.pojo;

import java.util.ArrayList;
import java.util.HashMap;

public class MonitorObject {

	private String username;
	private String password;
	private String monitorName;
	private String oneSightDomain;
	private String monitorType;
	private ArrayList<Integer> id;
	private String findComboSelection;
	private Integer monitorGroupId;
	private String findText;
	private String altType;
	private ArrayList<String> location;
	private String[] propertyKey;
	private String[] propertyValue;
	private HashMap<String, String> properties;
	private int sampleValue;
	private int sampleUnits;
	private boolean fromAPI;
	private Integer selectedTreeId;
	private Integer systemId;
	private String systemName;
	private int monitorId;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMonitorName() {
		return monitorName;
	}

	public void setMonitorName(String monitorName) {
		this.monitorName = monitorName;
	}

	public String getOneSightDomain() {
		return oneSightDomain;
	}

	public void setOneSightDomain(String oneSightDomain) {
		this.oneSightDomain = oneSightDomain;
	}

	public ArrayList<Integer> getId() {
		return id;
	}

	public void setId(ArrayList<Integer> id) {
		this.id = id;
	}

	public String getFindComboSelection() {
		return findComboSelection;
	}

	public void setFindComboSelection(String findComboSelection) {
		this.findComboSelection = findComboSelection;
	}

	public String getFindText() {
		return findText;
	}

	public void setFindText(String findText) {
		this.findText = findText;
	}

	public String getAltType() {
		return altType;
	}

	public void setAltType(String altType) {
		this.altType = altType;
	}

	public ArrayList<String> getLocation() {
		return location;
	}

	public void setLocation(ArrayList<String> location) {
		this.location = location;
	}

	public String[] getPropertyKey() {
		return propertyKey;
	}

	public void setPropertyKey(String[] propertyKey) {
		this.propertyKey = propertyKey;
	}

	public String[] getPropertyValue() {
		return propertyValue;
	}

	public void setPropertyValue(String[] propertyValue) {
		this.propertyValue = propertyValue;
	}

	public HashMap<String, String> getProperties() {
		return properties;
	}

	public void setProperties(HashMap<String, String> properties) {
		this.properties = properties;
	}

	public int getSampleValue() {
		return sampleValue;
	}

	public void setSampleValue(int sampleValue) {
		this.sampleValue = sampleValue;
	}

	public int getSampleUnits() {
		return sampleUnits;
	}

	public void setSampleUnits(int sampleUnits) {
		this.sampleUnits = sampleUnits;
	}

	public boolean isFromAPI() {
		return fromAPI;
	}

	public void setFromAPI(boolean fromAPI) {
		this.fromAPI = fromAPI;
	}

	public String getMonitorType() {
		return monitorType;
	}

	public void setMonitorType(String monitorType) {
		this.monitorType = monitorType;
	}

	public Integer getMonitorGroupId() {
		return monitorGroupId;
	}

	public void setMonitorGroupId(Integer monitorGroupId) {
		this.monitorGroupId = monitorGroupId;
	}

	public Integer getSelectedTreeId() {
		return selectedTreeId;
	}

	public void setSelectedTreeId(Integer selectedTreeId) {
		this.selectedTreeId = selectedTreeId;
	}

	public Integer getSystemId() {
		return systemId;
	}

	public void setSystemId(Integer systemId) {
		this.systemId = systemId;
	}

	public String getSystemName() {
		return systemName;
	}

	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	public int getMonitorId() {
		return monitorId;
	}

	public void setMonitorId(int monitorId) {
		this.monitorId = monitorId;
	}

}
