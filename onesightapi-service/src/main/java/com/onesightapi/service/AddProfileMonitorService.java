package com.onesightapi.service;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.onesightapi.pojo.MonitorGroupObject;
import com.onesightapi.pojo.MonitorObject;
import com.onesightapi.utility.MD5;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

public class AddProfileMonitorService {

	String jSessionId = "";
	String blend = "";

	public String process(MonitorObject monitorObject) {
		String result = "Error in API";
		try {
			Unirest.config().reset();
			Unirest.config().followRedirects(false);
			Unirest.config().connectTimeout(900000);
			Unirest.config().socketTimeout(900000);
			homePage(monitorObject);
			doLogin(monitorObject);
			monitorAdd(monitorObject);
			monitorAddTypeChanged(monitorObject);
			appliedMonitorWizardStart(monitorObject);
			appliedMonitorWizardStep2(monitorObject);
			wizardShowComponents(monitorObject);
			appliedMonitorWizardStep3(monitorObject);
			appliedMonitorWizardSaveSystem(monitorObject);
			appliedMonitorWizardCreate(monitorObject);
			monitorGroupSelectionSave(monitorObject);
			appliedMonitorSaveToDB(monitorObject);
			// accessControlSave(monitorObject);
			// attributePanelSave(monitorObject);
			// downtimeSelectionSave(monitorObject);
			/*
			 * if (monitorObject.getMonitorGroupId() != null &&
			 * monitorObject.getMonitorGroupId() > 0)
			 * monitorGroupSelectionSave(monitorObject);
			 */
			monitors(monitorObject);
			result = monitorsList(monitorObject);
		}

		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		finally {
			logout(monitorObject);
		}
		return result;
	}

	public void homePage(MonitorObject monitorObject) {

		HttpResponse<String> response = Unirest.get(monitorObject.getOneSightDomain() + "/login.jsp")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();

		String pattern1 = "JSESSIONID=";
		String pattern2 = ";";
		String text = response.getCookies().get(0).toString();
		Pattern p = Pattern.compile(Pattern.quote(pattern1) + "(.*?)" + Pattern.quote(pattern2));
		Matcher m = p.matcher(text);
		while (m.find()) {
			System.out.println(m.group(1));
			jSessionId = m.group(1);
		}

		String body = response.getBody();
		int i = body.indexOf("blend += '");
		int j = body.indexOf("document.login.passwordHash.value=hex_md5(blend)");
		blend = body.substring(i, j).replace("';", "").replace("blend += '", "").trim();
	}

	public void doLogin(MonitorObject monitorObject) {

		byte[] decoded = Base64.getDecoder().decode(monitorObject.getPassword());
		String decodedPassword = new String(decoded, StandardCharsets.UTF_8);

		String passBlend = decodedPassword + blend;
		String passwordHash = new MD5().calcMD5(passBlend);
		String requestURL = monitorObject.getOneSightDomain() + "/do_login.htm?action=login&name="
				+ monitorObject.getUsername() + "&passwordHashOld=" + monitorObject.getPassword() + "&passwordHash="
				+ passwordHash;
		System.out.println(requestURL);
		HttpResponse<String> response = Unirest
				.get(monitorObject.getOneSightDomain() + "/do_login.htm?action=login&name="
						+ monitorObject.getUsername() + "&passwordHashOld=" + monitorObject.getPassword()
						+ "&passwordHash=" + passwordHash)
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie", "JSESSIONID=" + jSessionId).header("Referer", monitorObject.getOneSightDomain() + ":8080/login.jsp")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();
	}

	public void monitorAdd(MonitorObject monitorObject) {
		System.out.println("inside monitorAdd");
		HttpResponse<String> response = Unirest
				.get(monitorObject.getOneSightDomain() + "/MonitorAdd.htm?fromMonitorGroup=false")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie",
						"SortDirection=monitorsTable~up|monitorGroupsTable~up|monitorgroupsTable~up|attributesTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up; SortTable=monitorsTable~2|monitorGroupsTable~2|monitorgroupsTable~1|attributesTable~0|alertsTable~1|locationItemsTable~1|smartLinkTable~0; "
								+ "JSESSIONID=" + jSessionId)
				.header("Referer", monitorObject.getOneSightDomain() + ":8080/configure.jsp").header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();

	}

	public void monitorAddTypeChanged(MonitorObject monitorObject) {
		System.out.println("inside monitorTypeChange");
		System.out.println(monitorObject.getOneSightDomain() + "/MonitorAddTypeChanged.htm?selectedTreeId="
						+ monitorObject.getSelectedTreeId() + "&selectedTreeName=" + monitorObject.getMonitorType());
		HttpResponse<String> response = Unirest
				.get(monitorObject.getOneSightDomain() + "/MonitorAddTypeChanged.htm?selectedTreeId="
						+ monitorObject.getSelectedTreeId() + "&selectedTreeName=" + monitorObject.getMonitorType())
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie",
						"SortDirection=monitorsTable~up|monitorGroupsTable~up|monitorgroupsTable~up|attributesTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up; SortTable=monitorsTable~2|monitorGroupsTable~2|monitorgroupsTable~1|attributesTable~0|alertsTable~1|locationItemsTable~1|smartLinkTable~0; "
								+ "JSESSIONID=" + jSessionId)
				.header("Referer", monitorObject.getOneSightDomain() + ":8080/config/monitors/monitorLeftTree.jsp")
				.header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();
		System.out.println("exit monitorTypeChange");
	}

	public void appliedMonitorWizardStart(MonitorObject monitorObject) {
		System.out.println("inside appliedMonitor1");
		HttpResponse<String> response = Unirest
				.get(monitorObject.getOneSightDomain() + "/AppliedMonitorWizardStart.htm")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie",
						"SortDirection=systemTable~up|attributesTable~up|monitorsTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up|metricListTable~up|metricTable~up|wizardSystemTable~up|locationTable~up|locationGroupsTable~up|inclusionsTable~up; SortTable=systemTable~2|attributesTable~0|monitorsTable~2|alertsTable~1|locationItemsTable~1|smartLinkTable~0|metricListTable~0|metricTable~1|wizardSystemTable~0|locationTable~1|locationGroupsTable~1|inclusionsTable~0; JSESSIONID="
								+ jSessionId)
				.header("Referer",
						monitorObject.getOneSightDomain() + ":8080/MonitorAddTypeChanged.htm?selectedTreeId=6&selectedTreeName=Microsoft%20Windows%20Monitor")
				.header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36")
				.asString();
		System.out.println("exit appliedMonitor1");
	}

	public void appliedMonitorWizardStep2(MonitorObject monitorObject) {
		System.out.println("inside appliedMonitor2");
		HttpResponse<String> response = Unirest
				.get(monitorObject.getOneSightDomain() + "/AppliedMonitorWizardStep2.htm?systemId="
						+ monitorObject.getSystemId() + "&name=" + monitorObject.getSystemName() + "&locationId=-1")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie",
						"SortDirection=systemTable~up|attributesTable~up|monitorsTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up|metricListTable~up|metricTable~up|wizardSystemTable~up|locationTable~up|locationGroupsTable~up|inclusionsTable~up; SortTable=systemTable~2|attributesTable~0|monitorsTable~2|alertsTable~1|locationItemsTable~1|smartLinkTable~0|metricListTable~0|metricTable~1|wizardSystemTable~0|locationTable~1|locationGroupsTable~1|inclusionsTable~0; JSESSIONID="
								+ jSessionId)
				.header("Referer", monitorObject.getOneSightDomain() + ":8080/AppliedMonitorWizardStart.htm")
				.header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36")
				.asString();
		System.out.println("exit appliedMonitor3");
	}

	public void wizardShowComponents(MonitorObject monitorObject) {
		System.out.println("inside wizard");
		HttpResponse<String> response = Unirest
				.get(monitorObject.getOneSightDomain() + "/config/systems/wizardShowComponents.jsp")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie",
						"SortDirection=systemTable~up|attributesTable~up|monitorsTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up|metricListTable~up|metricTable~up|wizardSystemTable~up|locationTable~up|locationGroupsTable~up|inclusionsTable~up; SortTable=systemTable~2|attributesTable~0|monitorsTable~2|alertsTable~1|locationItemsTable~1|smartLinkTable~0|metricListTable~0|metricTable~1|wizardSystemTable~0|locationTable~1|locationGroupsTable~1|inclusionsTable~0; JSESSIONID="
								+ jSessionId)
				.header("Referer",
						monitorObject.getOneSightDomain() + ":8080/AppliedMonitorWizardStep2.htm?systemId=1624&name=10.90.1.18&locationId=-1")
				.header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36")
				.asString();
		System.out.println("exit wizard");
	}

	public void appliedMonitorWizardStep3(MonitorObject monitorObject) {
		System.out.println("inside appliedMonitor3");
		HttpResponse<String> response = Unirest
				.post(monitorObject.getOneSightDomain() + "/AppliedMonitorWizardStep3.htm")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7")
				.header("Accept-Language", "en-US,en;q=0.9").header("Cache-Control", "max-age=0")
				.header("Connection", "keep-alive").header("Content-Type", "application/x-www-form-urlencoded")
				.header("Cookie",
						"SortDirection=systemTable~up|attributesTable~up|monitorsTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up|metricListTable~up|metricTable~up|wizardSystemTable~up|locationTable~up|locationGroupsTable~up|inclusionsTable~up; SortTable=systemTable~2|attributesTable~0|monitorsTable~2|alertsTable~1|locationItemsTable~1|smartLinkTable~0|metricListTable~0|metricTable~1|wizardSystemTable~0|locationTable~1|locationGroupsTable~1|inclusionsTable~0; JSESSIONID="
								+ jSessionId)
				.header("Origin", monitorObject.getOneSightDomain() + ":8080")
				.header("Referer", monitorObject.getOneSightDomain() + ":8080/config/systems/wizardShowComponents.jsp")
				.header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36")
				.field("systemObj.entityTypes[0].entities[0].keepChange", "on")
				.field("systemObj.entityTypes[0].entities[1].keepChange", "on")
				.field("systemObj.entityTypes[0].entities[2].keepChange", "on")
				.field("systemObj.entityTypes[0].entities[3].keepChange", "on")
				.field("systemObj.entityTypes[1].entities[0].keepChange", "on")
				.field("systemObj.entityTypes[1].entities[1].keepChange", "on")
				.field("systemObj.entityTypes[2].entities[0].keepChange", "on").asString();
		System.out.println("exit appliedMonitor3");
	}

	public void appliedMonitorWizardSaveSystem(MonitorObject monitorObject) {
		System.out.println("inside appliedMonitorSave");
		HttpResponse<String> response = Unirest
				.get(monitorObject.getOneSightDomain() + "/AppliedMonitorWizardSaveSystem.htm")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7")
				.header("Accept-Language", "en-US,en;q=0.9").header("Cache-Control", "max-age=0")
				.header("Connection", "keep-alive")
				.header("Cookie",
						"SortDirection=systemTable~up|attributesTable~up|monitorsTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up|metricListTable~up|metricTable~up|wizardSystemTable~up|locationTable~up|locationGroupsTable~up|inclusionsTable~up; SortTable=systemTable~2|attributesTable~0|monitorsTable~2|alertsTable~1|locationItemsTable~1|smartLinkTable~0|metricListTable~0|metricTable~1|wizardSystemTable~0|locationTable~1|locationGroupsTable~1|inclusionsTable~0; JSESSIONID="
								+ jSessionId)
				.header("Referer", monitorObject.getOneSightDomain() + ":8080/config/systems/wizardShowComponents.jsp")
				.header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36")
				.asString();
		System.out.println("exit appliedMonitorSave");
	}

	public void appliedMonitorWizardCreate(MonitorObject monitorObject) {
		System.out.println("inside appliedMonitorCreate");
		HttpResponse<String> response = Unirest
				.get(monitorObject.getOneSightDomain() + "/applied/AppliedMonitorWizardCreate.htm")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7")
				.header("Accept-Language", "en-US,en;q=0.9").header("Cache-Control", "max-age=0")
				.header("Connection", "keep-alive")
				.header("Cookie",
						"SortDirection=systemTable~up|attributesTable~up|monitorsTable~up|monitorGroupsTable~up|monitorgroupsTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up|metricListTable~up|wizardSystemTable~up|appliedMetricTable~up|appliedEntityTable~up; SortTable=systemTable~2|attributesTable~0|monitorsTable~2|monitorGroupsTable~2|monitorgroupsTable~1|alertsTable~1|locationItemsTable~1|smartLinkTable~0|metricListTable~0|wizardSystemTable~0|appliedMetricTable~1|appliedEntityTable~1; SortDirection=systemTable~up|attributesTable~up|monitorsTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up|metricListTable~up|metricTable~up|wizardSystemTable~up|locationTable~up|locationGroupsTable~up|inclusionsTable~up; SortTable=systemTable~2|attributesTable~0|monitorsTable~2|alertsTable~1|locationItemsTable~1|smartLinkTable~0|metricListTable~0|metricTable~1|wizardSystemTable~0|locationTable~1|locationGroupsTable~1|inclusionsTable~0; JSESSIONID="
								+ jSessionId)
				.header("Referer", monitorObject.getOneSightDomain() + ":8080/config/systems/wizardShowComponents.jsp")
				.header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36")
				.asString();
		System.out.println("exit appliedMonitorCreate");
	}

	public void appliedMonitorSaveToDB(MonitorObject monitorObject) {
		System.out.println("inside appliedMonitorSaveDB");
		HttpResponse<String> response = Unirest
				.get(monitorObject.getOneSightDomain() + "/applied/AppliedMonitorSaveToDB.htm?monitorObj.name="
						+ monitorObject.getMonitorName() + "&altype=" + monitorObject.getAltType() + "&msgformat=")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie",
						"SortDirection=systemTable~up|attributesTable~up|monitorsTable~up|monitorGroupsTable~up|monitorgroupsTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up|metricListTable~up|wizardSystemTable~up|appliedMetricTable~up|appliedEntityTable~up; SortTable=systemTable~2|attributesTable~0|monitorsTable~2|monitorGroupsTable~2|monitorgroupsTable~1|alertsTable~1|locationItemsTable~1|smartLinkTable~0|metricListTable~0|wizardSystemTable~0|appliedMetricTable~1|appliedEntityTable~1; SortDirection=systemTable~up|attributesTable~up|monitorsTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up|metricListTable~up|metricTable~up|wizardSystemTable~up|locationTable~up|locationGroupsTable~up|inclusionsTable~up; SortTable=systemTable~2|attributesTable~0|monitorsTable~2|alertsTable~1|locationItemsTable~1|smartLinkTable~0|metricListTable~0|metricTable~1|wizardSystemTable~0|locationTable~1|locationGroupsTable~1|inclusionsTable~0; JSESSIONID="
								+ jSessionId)
				.header("Referer", monitorObject.getOneSightDomain() + ":8080/applied/AppliedMonitorWizardCreate.htm")
				.header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36")
				.asString();
		System.out.println("exit appliedMonitorSaveDB");
	}

	public void monitorGroupSelectionSave(MonitorObject monitorObject) {
		HttpResponse<String> response = Unirest
				.get(monitorObject.getOneSightDomain()
						+ "/MonitorGroupSelectionSave.htm?monitorGroupSelection.selectedIds="
						+ monitorObject.getMonitorGroupId())
				.header("Accept", "*/*").header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie",
						"SortDirection=roleTable~up|monitorsTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up|attributesTable~up; SortTable=roleTable~1|monitorsTable~2|alertsTable~1|locationItemsTable~1|smartLinkTable~0|attributesTable~0; "
								+ "JSESSIONID=" + jSessionId)
				.header("Referer",
						monitorObject.getOneSightDomain() + ":8080/MonitorAddTypeChanged.htm?selectedTreeId=-2147483648&selectedTreeName="
								+ monitorObject.getMonitorType())
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();

	}

	public void monitors(MonitorObject monitorObject) {

		HttpResponse<String> response = Unirest.get(monitorObject.getOneSightDomain() + "/Monitors.htm")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie",
						"SortDirection=monitorGroupsTable~up|monitorgroupsTable~up|attributesTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up|monitorsTable~up; SortTable=monitorGroupsTable~2|monitorgroupsTable~1|attributesTable~0|alertsTable~1|locationItemsTable~1|smartLinkTable~0|monitorsTable~2; "
								+ "JSESSIONID=" + jSessionId)
				.header("Referer", monitorObject.getOneSightDomain() + ":8080/configure.jsp").header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();

	}

	public String monitorsList(MonitorObject monitorObject) {

		HttpResponse<String> response = Unirest
				.get(monitorObject.getOneSightDomain() + "/MonitorListAPI.htm?findComboSelection="
						+ monitorObject.getFindComboSelection() + "&findText=" + monitorObject.getFindText())
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie",
						"SortDirection=monitorGroupsTable~up|monitorgroupsTable~up|attributesTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up|monitorsTable~up; SortTable=monitorGroupsTable~2|monitorgroupsTable~1|attributesTable~0|alertsTable~1|locationItemsTable~1|smartLinkTable~0|monitorsTable~2; "
								+ "JSESSIONID=" + jSessionId)
				.header("Referer", monitorObject.getOneSightDomain() + ":8080/Monitors.htm").header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();

		return response.getBody();

	}

	public void logout(MonitorObject monitorObject) {

		HttpResponse<String> response = Unirest.get(monitorObject.getOneSightDomain() + "/logout.jsp")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie", "JSESSIONID=" + jSessionId)
				.header("Referer", monitorObject.getOneSightDomain() + "/Overview.htm")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();

	}

}
