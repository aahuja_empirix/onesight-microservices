package com.onesightapi.service;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.onesightapi.pojo.MonitorGroupObject;
import com.onesightapi.pojo.MonitorObject;
import com.onesightapi.utility.MD5;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

public class AddMonitorService {

	String jSessionId = "";
	String blend = "";

	public String process(MonitorObject monitorObject) {
		String result = "Error in API";
		try {
			// Unirest.config().followRedirects(false);
			homePage(monitorObject);
			doLogin(monitorObject);
			monitorAdd(monitorObject);
			monitorAddTypeChanged(monitorObject);
			monitorDedicated(monitorObject);
			monitorEditorSaveDetails(monitorObject);
			// accessControlSave(monitorObject);
			// attributePanelSave(monitorObject);
			// downtimeSelectionSave(monitorObject);
			if (monitorObject.getMonitorGroupId() != null && monitorObject.getMonitorGroupId() > 0)
				monitorGroupSelectionSave(monitorObject);
			if(monitorObject.getLocation() != null && monitorObject.getLocation().size() > 0)
				locationTabSave(monitorObject);
			monitorEditorSave(monitorObject);
			monitors(monitorObject);
			result = monitorsList(monitorObject);
		}

		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		finally {
			logout(monitorObject);
		}
		return result;
	}

	public void homePage(MonitorObject monitorObject) {

		HttpResponse<String> response = Unirest.get(monitorObject.getOneSightDomain() + "/login.jsp")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();

		System.out.println(
				"Cookeis ***************************************************************************************************************************************");
		// System.out.println(response.getCookies().get(0).toString().substring("JSESSIONID",";"));

		String pattern1 = "JSESSIONID=";
		String pattern2 = ";";
		String text = response.getCookies().get(0).toString();
		Pattern p = Pattern.compile(Pattern.quote(pattern1) + "(.*?)" + Pattern.quote(pattern2));
		Matcher m = p.matcher(text);
		while (m.find()) {
			System.out.println(m.group(1));
			jSessionId = m.group(1);
		}

		System.out.println(
				"Body ***************************************************************************************************************************************");
		System.out.println(response.getHeaders());

		System.out.println(
				"Body ***************************************************************************************************************************************");
		System.out.println(response.getBody());
		String body = response.getBody();
		int i = body.indexOf("blend += '");
		int j = body.indexOf("document.login.passwordHash.value=hex_md5(blend)");
		blend = body.substring(i, j).replace("';", "").replace("blend += '", "").trim();
	}

	public void doLogin(MonitorObject monitorObject) {

		byte[] decoded = Base64.getDecoder().decode(monitorObject.getPassword());
		String decodedPassword = new String(decoded, StandardCharsets.UTF_8);

		String passBlend = decodedPassword + blend;
		String passwordHash = new MD5().calcMD5(passBlend);
		String requestURL = monitorObject.getOneSightDomain() + "/do_login.htm?action=login&name="
				+ monitorObject.getUsername() + "&passwordHashOld=" + monitorObject.getPassword() + "&passwordHash="
				+ passwordHash;
		System.out.println(requestURL);
		HttpResponse<String> response = Unirest
				.get(monitorObject.getOneSightDomain() + "/do_login.htm?action=login&name="
						+ monitorObject.getUsername() + "&passwordHashOld=" + monitorObject.getPassword()
						+ "&passwordHash=" + passwordHash)
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie", "JSESSIONID=" + jSessionId).header("Referer", "http://10.90.1.3:8080/login.jsp")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();

		System.out.println(
				"Cookeis ***************************************************************************************************************************************");
		System.out.println(response.getCookies());

		System.out.println(
				"Body ***************************************************************************************************************************************");
		System.out.println(response.getHeaders());

		System.out.println(
				"Body ***************************************************************************************************************************************");
		System.out.println(response.getBody());
	}

	public void monitorAdd(MonitorObject monitorObject) {
		System.out.println("inside monitorAdd");
		HttpResponse<String> response = Unirest
				.get(monitorObject.getOneSightDomain() + "/MonitorAdd.htm?fromMonitorGroup=false")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie",
						"SortDirection=monitorsTable~up|monitorGroupsTable~up|monitorgroupsTable~up|attributesTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up; SortTable=monitorsTable~2|monitorGroupsTable~2|monitorgroupsTable~1|attributesTable~0|alertsTable~1|locationItemsTable~1|smartLinkTable~0; "
								+ "JSESSIONID=" + jSessionId)
				.header("Referer", "http://10.90.1.3:8080/configure.jsp").header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();
		System.out.println(response.getBody());

	}

	public void monitorAddTypeChanged(MonitorObject monitorObject) {

		HttpResponse<String> response = Unirest
				.get(monitorObject.getOneSightDomain()
						+ "/MonitorAddTypeChanged.htm?selectedTreeId=-2147483648&selectedTreeName="
						+ monitorObject.getMonitorType())
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie",
						"SortDirection=monitorsTable~up|monitorGroupsTable~up|monitorgroupsTable~up|attributesTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up; SortTable=monitorsTable~2|monitorGroupsTable~2|monitorgroupsTable~1|attributesTable~0|alertsTable~1|locationItemsTable~1|smartLinkTable~0; "
								+ "JSESSIONID=" + jSessionId)
				.header("Referer", "http://10.90.1.3:8080/config/monitors/monitorLeftTree.jsp")
				.header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();

	}

	public void monitorDedicated(MonitorObject monitorObject) {
		System.out.println("inside monitorDedicated");
		HttpResponse<String> response = Unirest.get(monitorObject.getOneSightDomain() + "/MonitorDedicated.htm")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie",
						"SortDirection=monitorsTable~up|monitorGroupsTable~up|monitorgroupsTable~up|attributesTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up; SortTable=monitorsTable~2|monitorGroupsTable~2|monitorgroupsTable~1|attributesTable~0|alertsTable~1|locationItemsTable~1|smartLinkTable~0; "
								+ "JSESSIONID=" + jSessionId)
				.header("Referer",
						"http://10.90.1.3:8080/MonitorAddTypeChanged.htm?selectedTreeId=-2147483648&selectedTreeName=" 
								+ monitorObject.getMonitorType())
				.header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();
		System.out.println("exiting monitorDedicated");
	}

	public void monitorEditorSaveDetails(MonitorObject monitorObject) {

		String propVal = "";
		String propKey = "";

		for (Map.Entry<String, String> entry : monitorObject.getProperties().entrySet()) {
			propKey = propKey + "&propertyKey=" + entry.getKey();
			propVal = propVal + "&propertyValue=" + entry.getValue();
		}

		propKey = propKey.trim().substring(0, propKey.length());
		propVal = propVal.trim().substring(0, propVal.length());
		String reqURL = monitorObject.getOneSightDomain() + "/MonitorEditorSaveDetails.htm?monitorObj.name="
				+ monitorObject.getMonitorName() + "&altype=" + monitorObject.getAltType() + propKey + propVal
				+ "&sampleValue=" + monitorObject.getSampleValue() + "&sampleUnits=" + monitorObject.getSampleUnits()
				+ "&fromAPI=" + monitorObject.isFromAPI();

		System.out.println(reqURL);

		System.out.println("inside monitorEditorSaveDetails");
		HttpResponse<String> response = Unirest
				.post(monitorObject.getOneSightDomain() + "/MonitorEditorSaveDetails.htm?monitorObj.name="
						+ monitorObject.getMonitorName() + "&altype=" + monitorObject.getAltType() + propKey + propVal
						+ "&sampleValue=" + monitorObject.getSampleValue() + "&sampleUnits="
						+ monitorObject.getSampleUnits() + "&fromAPI=" + monitorObject.isFromAPI())
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Cache-Control", "max-age=0")
				.header("Connection", "keep-alive").header("Content-Type", "application/x-www-form-urlencoded")
				.header("Cookie",
						"SortDirection=monitorsTable~up|monitorGroupsTable~up|monitorgroupsTable~up|attributesTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up; SortTable=monitorsTable~2|monitorGroupsTable~2|monitorgroupsTable~1|attributesTable~0|alertsTable~1|locationItemsTable~1|smartLinkTable~0; "
								+ "JSESSIONID=" + jSessionId)
				.header("Origin", "http://10.90.1.3:8080")
				.header("Referer", "http://10.90.1.3:8080/MonitorDedicated.htm")
				.header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				/*
				 * .field("propertyKey", "HostName").field("propertyValue",
				 * "10.90.1.3") .field("propertyKey",
				 * "PingMeasure").field("propertyValue", "PingAvgRTT")
				 * .field("propertyKey", "DataLength").field("propertyValue",
				 * "64").field("propertyKey", "PingTimeToLive")
				 * .field("propertyValue", "50").field("propertyKey",
				 * "Timeout").field("propertyValue", "5") .field("propertyKey",
				 * "Retries").field("propertyValue", "3").field("propertyKey",
				 * "On+Failure") .field("propertyValue",
				 * "Run+Traceroute").field("sampleValue",
				 * "5").field("sampleUnits", "60")
				 */
				.asString();
		System.out.println("exiting monitorEditorSaveDetails");

	}

	public void accessControlSave(MonitorObject monitorObject) {
		System.out.println("inside accessCOntrol");
		HttpResponse<String> response = Unirest.post(monitorObject.getOneSightDomain() + "/AccessControlSave.htm")
				.header("Accept", "*/*").header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Content-Type", "application/x-www-form-urlencoded")
				.header("Cookie",
						"SortDirection=monitorsTable~up|monitorGroupsTable~up|monitorgroupsTable~up|attributesTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up; SortTable=monitorsTable~2|monitorGroupsTable~2|monitorgroupsTable~1|attributesTable~0|alertsTable~1|locationItemsTable~1|smartLinkTable~0; "
								+ "JSESSIONID=" + jSessionId)
				.header("Origin", "http://10.90.1.3:8080")
				.header("Referer",
						"http://10.90.1.3:8080/MonitorAddTypeChanged.htm?selectedTreeId=-2147483648&selectedTreeName=" 
								+ monitorObject.getMonitorType())
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.field("acl[0].owner", "on").field("acl[0].global", "off").field("acl[0].readable", "on")
				.field("acl[0].writable", "on").field("acl[0].deletable", "on").asString();
		System.out.println("exiting accessCOntrol");
	}

	public void attributePanelSave(MonitorObject monitorObject) {
		System.out.println("inside attributePanel");
		HttpResponse<String> response = Unirest.post(monitorObject.getOneSightDomain() + "/AttributePanelSave.htm")
				.header("Accept", "*/*").header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Content-Length", "0").header("Content-Type", "application/x-www-form-urlencoded")
				.header("Cookie",
						"SortDirection=monitorsTable~up|monitorGroupsTable~up|monitorgroupsTable~up|attributesTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up; SortTable=monitorsTable~2|monitorGroupsTable~2|monitorgroupsTable~1|attributesTable~0|alertsTable~1|locationItemsTable~1|smartLinkTable~0; "
								+ "JSESSIONID=" + jSessionId)
				.header("Origin", "http://10.90.1.3:8080")
				.header("Referer",
						"http://10.90.1.3:8080/MonitorAddTypeChanged.htm?selectedTreeId=-2147483648&selectedTreeName=" 
								+ monitorObject.getMonitorType())
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();
		System.out.println("exiting attributePanel");
	}

	public void downtimeSelectionSave(MonitorObject monitorObject) {
		System.out.println("inside downtime");
		HttpResponse<String> response = Unirest.get(monitorObject.getOneSightDomain() + "/DowntimeSelectionSave.htm")
				.header("Accept", "*/*").header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie",
						"SortDirection=monitorsTable~up|monitorGroupsTable~up|monitorgroupsTable~up|attributesTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up; SortTable=monitorsTable~2|monitorGroupsTable~2|monitorgroupsTable~1|attributesTable~0|alertsTable~1|locationItemsTable~1|smartLinkTable~0; "
								+ "JSESSIONID=" + jSessionId)
				.header("Referer",
						"http://10.90.1.3:8080/MonitorAddTypeChanged.htm?selectedTreeId=-2147483648&selectedTreeName=" 
								+ monitorObject.getMonitorType())
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();
		System.out.println("exiting downtime");
	}

	public void monitorGroupSelectionSave(MonitorObject monitorObject) {

		HttpResponse<String> response = Unirest
				.get(monitorObject.getOneSightDomain() + "/MonitorGroupSelectionSave.htm?monitorGroupSelection.selectedIds="
						+ monitorObject.getMonitorGroupId())
				.header("Accept", "*/*").header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie",
						"SortDirection=roleTable~up|monitorsTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up|attributesTable~up; SortTable=roleTable~1|monitorsTable~2|alertsTable~1|locationItemsTable~1|smartLinkTable~0|attributesTable~0; "
								+ "JSESSIONID=" + jSessionId)
				.header("Referer",
						"http://10.90.1.3:8080/MonitorAddTypeChanged.htm?selectedTreeId=-2147483648&selectedTreeName=" 
								+ monitorObject.getMonitorType())
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();

	}

	public void locationTabSave(MonitorObject monitorObject) {

		String temp = "";

		for (String location : monitorObject.getLocation()) {
			temp = temp + "locations=" + location + "&";
		}
		temp = temp.trim().substring(0, temp.length() - 1);
		System.out.println("inside location");
		System.out.println(monitorObject.getOneSightDomain() + "/LocationTabSave.htm?" + temp);
		HttpResponse<String> response = Unirest.post(monitorObject.getOneSightDomain() + "/LocationTabSave.htm?" + temp)
				.header("Accept", "*/*").header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Content-Type", "application/x-www-form-urlencoded")
				.header("Cookie",
						"SortDirection=monitorsTable~up|monitorGroupsTable~up|monitorgroupsTable~up|attributesTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up; SortTable=monitorsTable~2|monitorGroupsTable~2|monitorgroupsTable~1|attributesTable~0|alertsTable~1|locationItemsTable~1|smartLinkTable~0; "
								+ "JSESSIONID=" + jSessionId)
				.header("Origin", "http://10.90.1.3:8080")
				.header("Referer",
						"http://10.90.1.3:8080/MonitorAddTypeChanged.htm?selectedTreeId=-2147483648&selectedTreeName=" 
								+ monitorObject.getMonitorType())
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();
		System.out.println("inside location");
	}

	public void monitorEditorSave(MonitorObject monitorObject) {
		System.out.println("inside monitorEditorSave");
		HttpResponse<String> response = Unirest.get(monitorObject.getOneSightDomain() + "/MonitorEditorSave.htm")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie",
						"SortDirection=monitorsTable~up|monitorGroupsTable~up|monitorgroupsTable~up|attributesTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up; SortTable=monitorsTable~2|monitorGroupsTable~2|monitorgroupsTable~1|attributesTable~0|alertsTable~1|locationItemsTable~1|smartLinkTable~0; "
								+ "JSESSIONID=" + jSessionId)
				.header("Referer",
						"http://10.90.1.3:8080/MonitorAddTypeChanged.htm?selectedTreeId=-2147483648&selectedTreeName=" 
								+ monitorObject.getMonitorType())
				.header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();
		System.out.println("exiting monitorEditorSave");
	}

	public void monitors(MonitorObject monitorObject) {

		HttpResponse<String> response = Unirest.get(monitorObject.getOneSightDomain() + "/Monitors.htm")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie",
						"SortDirection=monitorGroupsTable~up|monitorgroupsTable~up|attributesTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up|monitorsTable~up; SortTable=monitorGroupsTable~2|monitorgroupsTable~1|attributesTable~0|alertsTable~1|locationItemsTable~1|smartLinkTable~0|monitorsTable~2; "
								+ "JSESSIONID=" + jSessionId)
				.header("Referer", "http://10.90.1.3:8080/configure.jsp").header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();

	}

	public String monitorsList(MonitorObject monitorObject) {

		HttpResponse<String> response = Unirest
				.get(monitorObject.getOneSightDomain() + "/MonitorListAPI.htm?findComboSelection="
						+ monitorObject.getFindComboSelection() + "&findText=" + monitorObject.getFindText())
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie",
						"SortDirection=monitorGroupsTable~up|monitorgroupsTable~up|attributesTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up|monitorsTable~up; SortTable=monitorGroupsTable~2|monitorgroupsTable~1|attributesTable~0|alertsTable~1|locationItemsTable~1|smartLinkTable~0|monitorsTable~2; "
								+ "JSESSIONID=" + jSessionId)
				.header("Referer", "http://10.90.1.3:8080/Monitors.htm").header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();

		return response.getBody();

	}

	public void logout(MonitorObject monitorObject) {

		HttpResponse<String> response = Unirest.get(monitorObject.getOneSightDomain() + "/logout.jsp")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie", "JSESSIONID=" + jSessionId)
				.header("Referer", monitorObject.getOneSightDomain() + "/Overview.htm")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();

	}

}
