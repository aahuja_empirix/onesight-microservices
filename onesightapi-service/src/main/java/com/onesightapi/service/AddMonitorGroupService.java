package com.onesightapi.service;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.onesightapi.pojo.MonitorGroupObject;
import com.onesightapi.utility.MD5;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

public class AddMonitorGroupService {

	String jSessionId = "";
	String blend = "";

	public String process(MonitorGroupObject addMonitorGroupObject) {
		String result = "Error in API";
		try {
			// Unirest.config().followRedirects(false);
			homePage(addMonitorGroupObject);
			doLogin(addMonitorGroupObject);
			monitorGroupAdd(addMonitorGroupObject);
			attributePanelSave(addMonitorGroupObject);
			downtimeSelectionSave(addMonitorGroupObject);
			monitorGroupSave(addMonitorGroupObject);
			result = monitorGroupsList(addMonitorGroupObject);
		}

		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		finally {
			logout(addMonitorGroupObject);
		}
		return result;
	}

	public void homePage(MonitorGroupObject addMonitorGroupObject) {

		HttpResponse<String> response = Unirest.get(addMonitorGroupObject.getOneSightDomain() + "/login.jsp")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();

		System.out.println(
				"Cookeis ***************************************************************************************************************************************");
		// System.out.println(response.getCookies().get(0).toString().substring("JSESSIONID",";"));

		String pattern1 = "JSESSIONID=";
		String pattern2 = ";";
		String text = response.getCookies().get(0).toString();
		Pattern p = Pattern.compile(Pattern.quote(pattern1) + "(.*?)" + Pattern.quote(pattern2));
		Matcher m = p.matcher(text);
		while (m.find()) {
			System.out.println(m.group(1));
			jSessionId = m.group(1);
		}

		System.out.println(
				"Body ***************************************************************************************************************************************");
		System.out.println(response.getHeaders());

		System.out.println(
				"Body ***************************************************************************************************************************************");
		System.out.println(response.getBody());
		String body = response.getBody();
		int i = body.indexOf("blend += '");
		int j = body.indexOf("document.login.passwordHash.value=hex_md5(blend)");
		blend = body.substring(i, j).replace("';", "").replace("blend += '", "").trim();
	}

	public void doLogin(MonitorGroupObject addMonitorGroupObject) throws UnsupportedEncodingException {

		byte[] decoded = Base64.getDecoder().decode(addMonitorGroupObject.getPassword());
		String decodedPassword = new String(decoded, StandardCharsets.UTF_8);

		String passBlend = decodedPassword + blend;
		String passwordHash = new MD5().calcMD5(passBlend);

		HttpResponse<String> response = Unirest
				.get(addMonitorGroupObject.getOneSightDomain() + "/do_login.htm?action=login&name="
						+ addMonitorGroupObject.getUsername() + "&passwordHashOld="
						+ addMonitorGroupObject.getPassword() + "&passwordHash=" + passwordHash)
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie", "JSESSIONID=" + jSessionId).header("Referer", "http://10.90.1.3:8080/login.jsp")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();

		System.out.println(
				"Cookeis ***************************************************************************************************************************************");
		System.out.println(response.getCookies());

		System.out.println(
				"Body ***************************************************************************************************************************************");
		System.out.println(response.getHeaders());

		System.out.println(
				"Body ***************************************************************************************************************************************");
		System.out.println(response.getBody());
	}

	public void monitorGroupAdd(MonitorGroupObject addMonitorGroupObject) {
		HttpResponse<String> response = Unirest.get(addMonitorGroupObject.getOneSightDomain() + "/MonitorGroupAdd.htm")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie",
						"JSESSIONID=" + jSessionId
								+ "; SortDirection=monitorGroupsTable~up; SortTable=monitorGroupsTable~2;")
				.header("Referer", "http://10.90.1.3:8080/configure.jsp")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();
	}

	public void attributePanelSave(MonitorGroupObject addMonitorGroupObject) {
		HttpResponse<String> response = Unirest
				.post(addMonitorGroupObject.getOneSightDomain() + "/AttributePanelSave.htm").header("Accept", "*/*")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Content-Type", "application/x-www-form-urlencoded")
				.header("Cookie",
						"JSESSIONID=" + jSessionId
								+ "; SortDirection=monitorGroupsTable~up|monitorgroupsTable~up|attributesTable~up; SortTable=monitorGroupsTable~2|monitorgroupsTable~1|attributesTable~0;")
				.header("Origin", "http://10.90.1.3:8080")
				.header("Referer", "http://10.90.1.3:8080/MonitorGroupAdd.htm")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();

	}

	public void downtimeSelectionSave(MonitorGroupObject addMonitorGroupObject) {
		HttpResponse<String> response = Unirest
				.get(addMonitorGroupObject.getOneSightDomain() + "/DowntimeSelectionSave.htm").header("Accept", "*/*")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie",
						"JSESSIONID=" + jSessionId
								+ "; SortDirection=monitorGroupsTable~up|monitorgroupsTable~up|attributesTable~up; SortTable=monitorGroupsTable~2|monitorgroupsTable~1|attributesTable~0;")
				.header("Referer", "http://10.90.1.3:8080/MonitorGroupAdd.htm")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();

	}

	public void monitorGroupSave(MonitorGroupObject addMonitorGroupObject) {
		HttpResponse<String> response = Unirest
				.post(addMonitorGroupObject.getOneSightDomain() + "/MonitorGroupSave.htm?monitorGroup.name="
						+ addMonitorGroupObject.getMonitorGroupName() + "&monitorGroup.description="
						+ addMonitorGroupObject.getMonitorGroupDesc()
						+ "&monitorGroup.smartLinkText=Click%20to%20view%20the%20SmartLink%20for%20this%20Monitor%20Group")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Cache-Control", "max-age=0")
				.header("Connection", "keep-alive").header("Content-Type", "application/x-www-form-urlencoded")
				.header("Cookie",
						"JSESSIONID=" + jSessionId
								+ "; SortDirection=monitorGroupsTable~up|monitorgroupsTable~up|attributesTable~up; SortTable=monitorGroupsTable~2|monitorgroupsTable~1|attributesTable~0;")
				.header("Origin", addMonitorGroupObject.getOneSightDomain())
				.header("Referer", addMonitorGroupObject.getOneSightDomain() + "/MonitorGroupAdd.htm")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.field("monitorGroupObj.name", addMonitorGroupObject.getMonitorGroupName())
				.field("monitorGroupObj.description", addMonitorGroupObject.getMonitorGroupDesc())
				.field("chartSource", "-1").field("monitorGroupObj.warningResponse", "")
				.field("monitorGroupObj.errorResponse", "").field("monitorGroupObj.goodResponse", "")
				.field("smartLinkUrl", "http://")
				.field("smartLinkRolloverText", "Click+to+view+the+SmartLink+for+this+Monitor+Group")
				.field("smartLinkNewBrowser", "true").field("_smartLinkNewBrowser", "on").asString();
	}

	public String monitorGroupsList(MonitorGroupObject addMonitorGroupObject) {

		HttpResponse<String> response = Unirest
				.get(addMonitorGroupObject.getOneSightDomain() + "/MonitorGroupsListAPI.htm")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Cache-Control", "max-age=0")
				.header("Connection", "keep-alive")
				.header("Cookie",
						"SortDirection=monitorGroupsTable~up|monitorgroupsTable~up|attributesTable~up; SortTable=monitorGroupsTable~2|monitorgroupsTable~1|attributesTable~0; JSESSIONID="
								+ jSessionId)
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();

		return response.getBody();

	}

	public void logout(MonitorGroupObject addMonitorGroupObject) {

		HttpResponse<String> response = Unirest.get(addMonitorGroupObject.getOneSightDomain() + "/logout.jsp")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie", "JSESSIONID=" + jSessionId)
				.header("Referer", addMonitorGroupObject.getOneSightDomain() + "/Overview.htm")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();

	}

}
