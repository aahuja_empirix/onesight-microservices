package com.onesightapi.service;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.onesightapi.pojo.MonitorObject;
import com.onesightapi.utility.MD5;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

public class DeleteMonitorService {

	String jSessionId = "";
	String blend = "";

	public String process(MonitorObject deleteMonitor) {
		String result = "Error in API";
		try {
			// Unirest.config().followRedirects(false);
			homePage(deleteMonitor);
			doLogin(deleteMonitor);
			monitors(deleteMonitor);
			delete(deleteMonitor);
			result = monitorsList(deleteMonitor);
		}

		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		finally {
			logout(deleteMonitor);
		}
		return result;
	}

	public void homePage(MonitorObject deleteMonitor) {

		HttpResponse<String> response = Unirest.get(deleteMonitor.getOneSightDomain() + "/login.jsp")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();

		System.out.println(
				"Cookeis ***************************************************************************************************************************************");
		// System.out.println(response.getCookies().get(0).toString().substring("JSESSIONID",";"));

		String pattern1 = "JSESSIONID=";
		String pattern2 = ";";
		String text = response.getCookies().get(0).toString();
		Pattern p = Pattern.compile(Pattern.quote(pattern1) + "(.*?)" + Pattern.quote(pattern2));
		Matcher m = p.matcher(text);
		while (m.find()) {
			System.out.println(m.group(1));
			jSessionId = m.group(1);
		}

		System.out.println(
				"Body ***************************************************************************************************************************************");
		System.out.println(response.getHeaders());

		System.out.println(
				"Body ***************************************************************************************************************************************");
		System.out.println(response.getBody());
		String body = response.getBody();
		int i = body.indexOf("blend += '");
		int j = body.indexOf("document.login.passwordHash.value=hex_md5(blend)");
		blend = body.substring(i, j).replace("';", "").replace("blend += '", "").trim();
	}

	public void doLogin(MonitorObject deleteMonitor) {

		byte[] decoded = Base64.getDecoder().decode(deleteMonitor.getPassword());
		String decodedPassword = new String(decoded, StandardCharsets.UTF_8);

		String passBlend = decodedPassword + blend;
		String passwordHash = new MD5().calcMD5(passBlend);

		HttpResponse<String> response = Unirest
				.get(deleteMonitor.getOneSightDomain() + "/do_login.htm?action=login&name="
						+ deleteMonitor.getUsername() + "&passwordHashOld=" + deleteMonitor.getPassword()
						+ "&passwordHash=" + passwordHash)
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie", "JSESSIONID=" + jSessionId).header("Referer", "http://10.90.1.3:8080/login.jsp")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();

		System.out.println(
				"Cookeis ***************************************************************************************************************************************");
		System.out.println(response.getCookies());

		System.out.println(
				"Body ***************************************************************************************************************************************");
		System.out.println(response.getHeaders());

		System.out.println(
				"Body ***************************************************************************************************************************************");
		System.out.println(response.getBody());
	}

	public void monitors(MonitorObject monitorObject) {

		HttpResponse<String> response = Unirest.get(monitorObject.getOneSightDomain() + "/Monitors.htm")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie",
						"SortDirection=monitorGroupsTable~up|monitorgroupsTable~up|attributesTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up|monitorsTable~up; SortTable=monitorGroupsTable~2|monitorgroupsTable~1|attributesTable~0|alertsTable~1|locationItemsTable~1|smartLinkTable~0|monitorsTable~2; "
								+ "JSESSIONID=" + jSessionId)
				.header("Referer", "http://10.90.1.3:8080/configure.jsp").header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();
		System.out.println("inside monitors");

	}

	public void delete(MonitorObject deleteMonitor) {

		String temp = "";

		for (Integer monitorId : deleteMonitor.getId()) {
			temp = temp + "monitorIds=" + monitorId + "&";
		}

		temp = temp.trim().substring(0, temp.length() - 1);

		HttpResponse<String> response = Unirest
				.get(deleteMonitor.getOneSightDomain() + "/MonitorRequestRemove.htm?" + temp)
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie",
						"SortDirection=monitorsTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up|attributesTable~up|monitorGroupsTable~up|monitorgroupsTable~up|existingMonitorsTable~up|locationTable~up|locationGroupsTable~up|inclusionsTable~up|setsTable~up|valuesTable~up|viewsTable~up|objectsTable~up|schedulesTableByDT~up|monitorsTableByDT~up; SortTable=monitorsTable~2|alertsTable~1|locationItemsTable~1|smartLinkTable~0|attributesTable~0|monitorGroupsTable~2|monitorgroupsTable~1|existingMonitorsTable~0|locationTable~1|locationGroupsTable~1|inclusionsTable~0|setsTable~1|valuesTable~1|viewsTable~1|objectsTable~1|schedulesTableByDT~0|monitorsTableByDT~0; JSESSIONID="
								+ jSessionId)
				.header("Referer", "http://10.90.1.3:8080/Monitors.htm").header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();
		System.out.println("inside deletemonitors");

	}

	public String monitorsList(MonitorObject monitorObject) {

		HttpResponse<String> response = Unirest
				.get(monitorObject.getOneSightDomain() + "/MonitorListAPI.htm?findComboSelection="
						+ monitorObject.getFindComboSelection() + "&findText=" + monitorObject.getFindText())
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie",
						"SortDirection=monitorGroupsTable~up|monitorgroupsTable~up|attributesTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up|monitorsTable~up; SortTable=monitorGroupsTable~2|monitorgroupsTable~1|attributesTable~0|alertsTable~1|locationItemsTable~1|smartLinkTable~0|monitorsTable~2; "
								+ "JSESSIONID=" + jSessionId)
				.header("Referer", "http://10.90.1.3:8080/Monitors.htm").header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();
		System.out.println("inside deletemonitors");
		return response.getBody();

	}

	public void logout(MonitorObject deleteMonitor) {

		HttpResponse<String> response = Unirest.get(deleteMonitor.getOneSightDomain() + "/logout.jsp")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie", "JSESSIONID=" + jSessionId)
				.header("Referer", deleteMonitor.getOneSightDomain() + "/Overview.htm")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();

	}

}
