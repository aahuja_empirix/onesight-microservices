package com.onesightapi.service;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.onesightapi.pojo.MonitorGroupObject;
import com.onesightapi.utility.MD5;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

public class DeleteMonitorGroupService {

	String jSessionId = "";
	String blend = "";

	public String process(MonitorGroupObject deleteMonitorGroup) {
		String result = "Error in API";
		try {
			// Unirest.config().followRedirects(false);
			homePage(deleteMonitorGroup);
			doLogin(deleteMonitorGroup);
			monitorGroups(deleteMonitorGroup);
			delete(deleteMonitorGroup);
			result = monitorGroupsList(deleteMonitorGroup);
		}

		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		finally {
			logout(deleteMonitorGroup);
		}
		return result;
	}

	public void homePage(MonitorGroupObject deleteMonitorGroup) {

		HttpResponse<String> response = Unirest.get(deleteMonitorGroup.getOneSightDomain() + "/login.jsp")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();

		System.out.println(
				"Cookeis ***************************************************************************************************************************************");
		// System.out.println(response.getCookies().get(0).toString().substring("JSESSIONID",";"));

		String pattern1 = "JSESSIONID=";
		String pattern2 = ";";
		String text = response.getCookies().get(0).toString();
		Pattern p = Pattern.compile(Pattern.quote(pattern1) + "(.*?)" + Pattern.quote(pattern2));
		Matcher m = p.matcher(text);
		while (m.find()) {
			System.out.println(m.group(1));
			jSessionId = m.group(1);
		}

		System.out.println(
				"Body ***************************************************************************************************************************************");
		System.out.println(response.getHeaders());

		System.out.println(
				"Body ***************************************************************************************************************************************");
		System.out.println(response.getBody());
		String body = response.getBody();
		int i = body.indexOf("blend += '");
		int j = body.indexOf("document.login.passwordHash.value=hex_md5(blend)");
		blend = body.substring(i, j).replace("';", "").replace("blend += '", "").trim();
	}

	public void doLogin(MonitorGroupObject deleteMonitorGroup) {

		byte[] decoded = Base64.getDecoder().decode(deleteMonitorGroup.getPassword());
		String decodedPassword = new String(decoded, StandardCharsets.UTF_8);

		String passBlend = decodedPassword + blend;
		String passwordHash = new MD5().calcMD5(passBlend);

		HttpResponse<String> response = Unirest
				.get(deleteMonitorGroup.getOneSightDomain() + "/do_login.htm?action=login&name="
						+ deleteMonitorGroup.getUsername() + "&passwordHashOld=" + deleteMonitorGroup.getPassword()
						+ "&passwordHash=" + passwordHash)
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie", "JSESSIONID=" + jSessionId).header("Referer", "http://10.90.1.3:8080/login.jsp")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();

		System.out.println(
				"Cookeis ***************************************************************************************************************************************");
		System.out.println(response.getCookies());

		System.out.println(
				"Body ***************************************************************************************************************************************");
		System.out.println(response.getHeaders());

		System.out.println(
				"Body ***************************************************************************************************************************************");
		System.out.println(response.getBody());
	}

	public void monitorGroups(MonitorGroupObject deleteMonitorGroup) {

		HttpResponse<String> response = Unirest.get(deleteMonitorGroup.getOneSightDomain() + "/MonitorGroups.htm")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Cache-Control", "max-age=0")
				.header("Connection", "keep-alive")
				.header("Cookie",
						"SortDirection=monitorGroupsTable~up|monitorgroupsTable~up|attributesTable~up; SortTable=monitorGroupsTable~2|monitorgroupsTable~1|attributesTable~0; JSESSIONID="
								+ jSessionId)
				.header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();
	}

	public void delete(MonitorGroupObject deleteMonitorGroup) {

		String temp = "";

		for (Integer monitorGroupId : deleteMonitorGroup.getId()) {
			temp = temp + "monitorGroupIds=" + monitorGroupId + "&";
		}

		temp = temp.trim().substring(0, temp.length() - 1);
		System.out.println(deleteMonitorGroup.getOneSightDomain() + "/MonitorGroupRemove.htm?" + temp);
		HttpResponse<String> response = Unirest
				.get(deleteMonitorGroup.getOneSightDomain() + "/MonitorGroupRemove.htm?" + temp)
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie",
						"SortDirection=monitorGroupsTable~up|monitorgroupsTable~up|attributesTable~up; SortTable=monitorGroupsTable~2|monitorgroupsTable~1|attributesTable~0; JSESSIONID="
								+ jSessionId)
				.header("Referer", "http://10.90.1.3:8080/MonitorGroups.htm").header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();

	}

	public String monitorGroupsList(MonitorGroupObject addMonitorGroupObject) {

		HttpResponse<String> response = Unirest
				.get(addMonitorGroupObject.getOneSightDomain() + "/MonitorGroupsListAPI.htm")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Cache-Control", "max-age=0")
				.header("Connection", "keep-alive")
				.header("Cookie",
						"SortDirection=monitorGroupsTable~up|monitorgroupsTable~up|attributesTable~up; SortTable=monitorGroupsTable~2|monitorgroupsTable~1|attributesTable~0; JSESSIONID="
								+ jSessionId)
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();

		return response.getBody();

	}

	public void logout(MonitorGroupObject deleteMonitorGroup) {

		HttpResponse<String> response = Unirest.get(deleteMonitorGroup.getOneSightDomain() + "/logout.jsp")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie", "JSESSIONID=" + jSessionId)
				.header("Referer", deleteMonitorGroup.getOneSightDomain() + "/Overview.htm")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();

	}

}
