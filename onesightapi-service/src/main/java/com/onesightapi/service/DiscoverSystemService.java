package com.onesightapi.service;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.onesightapi.pojo.SystemObject;
import com.onesightapi.utility.MD5;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

public class DiscoverSystemService {

	String jSessionId = "";
	String blend = "";

	public String process(SystemObject systemObject) {
		String result = "Error in API";
		try {
			Unirest.config().reset();
			Unirest.config().followRedirects(false);
			Unirest.config().connectTimeout(900000);
			Unirest.config().socketTimeout(900000);
			homePage(systemObject);
			doLogin(systemObject);
			modifySystem(systemObject);
			systemDetails(systemObject);
			stateMachineSelect(systemObject);
			systemSaveDetails(systemObject);
			findAgentList(systemObject);

			findSystemComponentsSetup(systemObject);
			// startSystemDiscovery(systemObject);
			findComponents(systemObject);
			String res = systemDiscoveryStatus(systemObject);
			if (res.contains("aStatus.push(\"Discovery finished")) {
				systemFindComponentsRemoved(systemObject);
				systemUpdateTree(systemObject);
				systemDetails(systemObject);
				stateMachineSelect(systemObject);
				systemList(systemObject);
				systemSave(systemObject);
			}

			// findJDBCSystemComponentsSetup(systemObject);
			// startSystemDiscovery(systemObject);
			// findComponents(systemObject);
			// systemDiscoveryStatus(systemObject);
			// systems(systemObject);
			// result = systemList(systemObject);
		}

		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		finally {
			logout(systemObject);
		}
		return result;
	}

	public void homePage(SystemObject systemObject) {

		HttpResponse<String> response = Unirest.get(systemObject.getOneSightDomain() + "/login.jsp")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();

		String pattern1 = "JSESSIONID=";
		String pattern2 = ";";
		String text = response.getCookies().get(0).toString();
		Pattern p = Pattern.compile(Pattern.quote(pattern1) + "(.*?)" + Pattern.quote(pattern2));
		Matcher m = p.matcher(text);
		while (m.find()) {
			System.out.println(m.group(1));
			jSessionId = m.group(1);
		}

		String body = response.getBody();
		int i = body.indexOf("blend += '");
		int j = body.indexOf("document.login.passwordHash.value=hex_md5(blend)");
		blend = body.substring(i, j).replace("';", "").replace("blend += '", "").trim();
	}

	public void doLogin(SystemObject systemObject) throws UnsupportedEncodingException {

		byte[] decoded = Base64.getDecoder().decode(systemObject.getPassword());
		String decodedPassword = new String(decoded, StandardCharsets.UTF_8);

		String passBlend = decodedPassword + blend;
		String passwordHash = new MD5().calcMD5(passBlend);

		HttpResponse<String> response = Unirest
				.get(systemObject.getOneSightDomain() + "/do_login.htm?action=login&name=" + systemObject.getUsername()
						+ "&passwordHashOld=" + systemObject.getPassword() + "&passwordHash=" + passwordHash)
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie", "JSESSIONID=" + jSessionId).header("Referer", "http://10.90.1.3:8080/login.jsp")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();
	}

	public void modifySystem(SystemObject systemObject) {
		// Unirest.setTimeouts(600000, 300000);
		HttpResponse<String> response = Unirest
				.get(systemObject.getOneSightDomain() + "/SystemModify.htm?systemID=" + systemObject.getId())
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie",
						"SortDirection=systemTable~up|attributesTable~up|monitorsTable~up|monitorGroupsTable~up|monitorgroupsTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up|metricListTable~up|wizardSystemTable~up|stateMachinesTable~up; SortTable=systemTable~2|attributesTable~0|monitorsTable~2|monitorGroupsTable~2|monitorgroupsTable~1|alertsTable~1|locationItemsTable~1|smartLinkTable~0|metricListTable~0|wizardSystemTable~0|stateMachinesTable~1; JSESSIONID="
								+ jSessionId)
				.header("Referer", "http://10.90.1.3:8080/configure.jsp").header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36")
				.asString();
	}

	public void systemDetails(SystemObject systemObject) {
		HttpResponse<String> response = Unirest
				.get(systemObject.getOneSightDomain() + "/SystemDetails.htm?tabIndex=0&entityType=&entity=&entityId=-1")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie",
						"SortDirection=systemTable~up|attributesTable~up; SortTable=systemTable~2|attributesTable~0; JSESSIONID="
								+ jSessionId)
				.header("Referer", "http://10.90.1.3:8080/SystemEditor.htm").header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36")
				.asString();

	}

	public void stateMachineSelect(SystemObject systemObject) {
		HttpResponse<String> response = Unirest
				.get(systemObject.getOneSightDomain() + "/StateMachineSelect.htm?selectedStateMachine=-1")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie",
						"SortDirection=systemTable~up|attributesTable~up; SortTable=systemTable~2|attributesTable~0; JSESSIONID="
								+ jSessionId)
				.header("Referer", "http://10.90.1.3:8080/SystemDetails.htm?tabIndex=0&entityType=&entity=&entityId=-1")
				.header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36")
				.asString();

	}

	public void systemSaveDetails(SystemObject systemObject) {
		HttpResponse<String> response = Unirest.post(systemObject.getOneSightDomain() + "/SystemSaveDetails.htm")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7")
				.header("Accept-Language", "en-US,en;q=0.9").header("Cache-Control", "max-age=0")
				.header("Connection", "keep-alive").header("Content-Type", "application/x-www-form-urlencoded")
				.header("Cookie",
						"SortDirection=systemTable~up|attributesTable~up; SortTable=systemTable~2|attributesTable~0; JSESSIONID="
								+ jSessionId)
				.header("Origin", "http://10.90.1.3:8080")
				.header("Referer", "http://10.90.1.3:8080/SystemDetails.htm?tabIndex=0&entityType=&entity=&entityId=-1")
				.header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36")
				.field("details.tabs[0].properties[0].value", systemObject.getSystemName())
				.field("details.tabs[0].properties[1].value", systemObject.getSystemLevel())
				.field("details.tabs[0].properties[2].value", systemObject.getSystemName())
				.field("details.tabs[0].properties[3].value", systemObject.getSystemName())
				.field("details.tabs[0].properties[4].value", systemObject.getSystemName())
				.field("details.tabs[0].properties[5].value",
						systemObject.getDetails().getSystemTab().get("locationId"))
				.field("propertyKey", "On+Failure")
				.field("details.tabs[0].properties[6].value", systemObject.getDetails().getSystemTab().get("onFailure"))
				.field("details.tabs[0].properties[7].value", systemObject.getOsType())
				.field("details.tabs[0].properties[8].value",
						systemObject.getDetails().getSystemTab().get("criticalState"))
				.field("details.tabs[0].properties[9].value", systemObject.getDetails().getSystemTab().get("goodState"))
				.field("details.tabs[0].properties[10].value",
						systemObject.getDetails().getSystemTab().get("warningState"))
				.field("details.tabs[1].properties[0].value",
						systemObject.getDetails().getSnmpTab().get("communityString"))
				.field("details.tabs[1].properties[1].value", systemObject.getDetails().getSnmpTab().get("timeout"))
				.field("details.tabs[1].properties[2].value", systemObject.getDetails().getSnmpTab().get("retry"))
				.field("details.tabs[1].properties[3].value", systemObject.getDetails().getSnmpTab().get("port"))
				.field("details.tabs[1].properties[4].value", systemObject.getDetails().getSnmpTab().get("snmpVersion"))
				.field("details.tabs[1].properties[5].value", systemObject.getDetails().getSnmpTab().get("authMode"))
				.field("details.tabs[1].properties[6].value", systemObject.getDetails().getSnmpTab().get("username"))
				.field("details.tabs[1].properties[7].value", systemObject.getDetails().getSnmpTab().get("password"))
				.field("details.tabs[1].properties[8].value", systemObject.getDetails().getSnmpTab().get("authType"))
				.field("details.tabs[1].properties[9].value", systemObject.getDetails().getSnmpTab().get("privacyMode"))
				.field("details.tabs[1].properties[10].value",
						systemObject.getDetails().getSnmpTab().get("privacyPassword"))
				.field("details.tabs[1].properties[11].value",
						systemObject.getDetails().getSnmpTab().get("contextName"))
				.field("details.tabs[2].properties[0].value",
						systemObject.getDetails().getSnmpTrapTab().get("communityString"))
				.field("details.tabs[2].properties[1].value",
						systemObject.getDetails().getSnmpTrapTab().get("snmpV3Config"))
				.field("details.tabs[3].properties[1].value",
						systemObject.getDetails().getVirtualAgentTab().get("connectionMethod"))
				.field("details.tabs[3].properties[2].value",
						systemObject.getDetails().getVirtualAgentTab().get("port"))
				.field("details.tabs[3].properties[3].value",
						systemObject.getDetails().getVirtualAgentTab().get("prompt"))
				.field("details.tabs[3].properties[4].value",
						systemObject.getDetails().getVirtualAgentTab().get("charset"))
				.field("details.tabs[3].properties[5].value",
						systemObject.getDetails().getVirtualAgentTab().get("remoteOS"))
				.field("details.tabs[3].properties[8].value",
						systemObject.getDetails().getVirtualAgentTab().get("remoteName"))
				.field("details.tabs[3].properties[9].value",
						systemObject.getDetails().getVirtualAgentTab().get("remotePassword"))
				.field("details.tabs[3].properties[10].value",
						systemObject.getDetails().getVirtualAgentTab().get("remoteKeyFile"))
				.field("details.tabs[3].properties[13].value",
						systemObject.getDetails().getVirtualAgentTab().get("remoteLoginTimeout"))
				.field("details.tabs[3].properties[14].value",
						systemObject.getDetails().getVirtualAgentTab().get("remoteLineTimeout"))
				.field("details.tabs[3].properties[15].value",
						systemObject.getDetails().getVirtualAgentTab().get("loginStateMachine"))
				.field("details.tabs[4].properties[0].value", systemObject.getDetails().getJmxTab().get("jmxAppName"))
				.field("details.tabs[4].properties[1].value",
						systemObject.getDetails().getJmxTab().get("jmxAppNameOther"))
				.field("details.tabs[4].properties[2].value", systemObject.getDetails().getJmxTab().get("port"))
				.field("details.tabs[4].properties[3].value",
						systemObject.getDetails().getJmxTab().get("jmxSecureConnection"))
				.field("details.tabs[4].properties[4].value", systemObject.getDetails().getJmxTab().get("jmxUsername"))
				.field("details.tabs[4].properties[5].value", systemObject.getDetails().getJmxTab().get("jmxPassword"))
				.field("details.tabs[4].properties[6].value", systemObject.getDetails().getJmxTab().get("jmxTrustFile"))
				.field("details.tabs[4].properties[7].value",
						systemObject.getDetails().getJmxTab().get("jmxTrustPassword"))
				.field("details.tabs[4].properties[8].value", systemObject.getDetails().getJmxTab().get("jmxKeyFile"))
				.field("details.tabs[4].properties[9].value",
						systemObject.getDetails().getJmxTab().get("jmxKeyPassword"))
				.field("details.tabs[5].properties[0].value",
						systemObject.getDetails().getWindowsNtTab().get("username"))
				.field("details.tabs[5].properties[1].value",
						systemObject.getDetails().getWindowsNtTab().get("password"))
				.field("details.tabs[5].properties[2].value", systemObject.getDetails().getWindowsNtTab().get("domain"))
				.asString();

	}
	
	public void systemSave(SystemObject systemObject) {
		HttpResponse<String> response = Unirest.post(systemObject.getOneSightDomain() + "/SystemSave.htm")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7")
				.header("Accept-Language", "en-US,en;q=0.9").header("Cache-Control", "max-age=0")
				.header("Connection", "keep-alive").header("Content-Type", "application/x-www-form-urlencoded")
				.header("Cookie",
						"SortDirection=systemTable~up|attributesTable~up; SortTable=systemTable~2|attributesTable~0; JSESSIONID="
								+ jSessionId)
				.header("Origin", "http://10.90.1.3:8080")
				.header("Referer", "http://10.90.1.3:8080/SystemDetails.htm?tabIndex=0&entityType=&entity=&entityId=-1")
				.header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36")
				.field("details.tabs[0].properties[0].value", systemObject.getSystemName())
				.field("details.tabs[0].properties[1].value", systemObject.getSystemLevel())
				.field("details.tabs[0].properties[2].value", systemObject.getSystemName())
				.field("details.tabs[0].properties[3].value", systemObject.getSystemName())
				.field("details.tabs[0].properties[4].value", systemObject.getSystemName())
				.field("details.tabs[0].properties[5].value",
						systemObject.getDetails().getSystemTab().get("locationId"))
				.field("propertyKey", "On+Failure")
				.field("details.tabs[0].properties[6].value", systemObject.getDetails().getSystemTab().get("onFailure"))
				.field("details.tabs[0].properties[7].value", systemObject.getOsType())
				.field("details.tabs[0].properties[8].value",
						systemObject.getDetails().getSystemTab().get("criticalState"))
				.field("details.tabs[0].properties[9].value", systemObject.getDetails().getSystemTab().get("goodState"))
				.field("details.tabs[0].properties[10].value",
						systemObject.getDetails().getSystemTab().get("warningState"))
				.field("details.tabs[1].properties[0].value",
						systemObject.getDetails().getSnmpTab().get("communityString"))
				.field("details.tabs[1].properties[1].value", systemObject.getDetails().getSnmpTab().get("timeout"))
				.field("details.tabs[1].properties[2].value", systemObject.getDetails().getSnmpTab().get("retry"))
				.field("details.tabs[1].properties[3].value", systemObject.getDetails().getSnmpTab().get("port"))
				.field("details.tabs[1].properties[4].value", systemObject.getDetails().getSnmpTab().get("snmpVersion"))
				.field("details.tabs[1].properties[5].value", systemObject.getDetails().getSnmpTab().get("authMode"))
				.field("details.tabs[1].properties[6].value", systemObject.getDetails().getSnmpTab().get("username"))
				.field("details.tabs[1].properties[7].value", systemObject.getDetails().getSnmpTab().get("password"))
				.field("details.tabs[1].properties[8].value", systemObject.getDetails().getSnmpTab().get("authType"))
				.field("details.tabs[1].properties[9].value", systemObject.getDetails().getSnmpTab().get("privacyMode"))
				.field("details.tabs[1].properties[10].value",
						systemObject.getDetails().getSnmpTab().get("privacyPassword"))
				.field("details.tabs[1].properties[11].value",
						systemObject.getDetails().getSnmpTab().get("contextName"))
				.field("details.tabs[2].properties[0].value",
						systemObject.getDetails().getSnmpTrapTab().get("communityString"))
				.field("details.tabs[2].properties[1].value",
						systemObject.getDetails().getSnmpTrapTab().get("snmpV3Config"))
				.field("details.tabs[3].properties[1].value",
						systemObject.getDetails().getVirtualAgentTab().get("connectionMethod"))
				.field("details.tabs[3].properties[2].value",
						systemObject.getDetails().getVirtualAgentTab().get("port"))
				.field("details.tabs[3].properties[3].value",
						systemObject.getDetails().getVirtualAgentTab().get("prompt"))
				.field("details.tabs[3].properties[4].value",
						systemObject.getDetails().getVirtualAgentTab().get("charset"))
				.field("details.tabs[3].properties[5].value",
						systemObject.getDetails().getVirtualAgentTab().get("remoteOS"))
				.field("details.tabs[3].properties[8].value",
						systemObject.getDetails().getVirtualAgentTab().get("remoteName"))
				.field("details.tabs[3].properties[9].value",
						systemObject.getDetails().getVirtualAgentTab().get("remotePassword"))
				.field("details.tabs[3].properties[10].value",
						systemObject.getDetails().getVirtualAgentTab().get("remoteKeyFile"))
				.field("details.tabs[3].properties[13].value",
						systemObject.getDetails().getVirtualAgentTab().get("remoteLoginTimeout"))
				.field("details.tabs[3].properties[14].value",
						systemObject.getDetails().getVirtualAgentTab().get("remoteLineTimeout"))
				.field("details.tabs[3].properties[15].value",
						systemObject.getDetails().getVirtualAgentTab().get("loginStateMachine"))
				.field("details.tabs[4].properties[0].value", systemObject.getDetails().getJmxTab().get("jmxAppName"))
				.field("details.tabs[4].properties[1].value",
						systemObject.getDetails().getJmxTab().get("jmxAppNameOther"))
				.field("details.tabs[4].properties[2].value", systemObject.getDetails().getJmxTab().get("port"))
				.field("details.tabs[4].properties[3].value",
						systemObject.getDetails().getJmxTab().get("jmxSecureConnection"))
				.field("details.tabs[4].properties[4].value", systemObject.getDetails().getJmxTab().get("jmxUsername"))
				.field("details.tabs[4].properties[5].value", systemObject.getDetails().getJmxTab().get("jmxPassword"))
				.field("details.tabs[4].properties[6].value", systemObject.getDetails().getJmxTab().get("jmxTrustFile"))
				.field("details.tabs[4].properties[7].value",
						systemObject.getDetails().getJmxTab().get("jmxTrustPassword"))
				.field("details.tabs[4].properties[8].value", systemObject.getDetails().getJmxTab().get("jmxKeyFile"))
				.field("details.tabs[4].properties[9].value",
						systemObject.getDetails().getJmxTab().get("jmxKeyPassword"))
				.field("details.tabs[5].properties[0].value",
						systemObject.getDetails().getWindowsNtTab().get("username"))
				.field("details.tabs[5].properties[1].value",
						systemObject.getDetails().getWindowsNtTab().get("password"))
				.field("details.tabs[5].properties[2].value", systemObject.getDetails().getWindowsNtTab().get("domain"))
				.asString();

	}

	public void findAgentList(SystemObject systemObject) {
		// Unirest.setTimeouts(0, 0);
		HttpResponse<String> response = Unirest.get(systemObject.getOneSightDomain() + "/SystemFindListAgents.htm")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie",
						"SortDirection=systemTable~up|attributesTable~up|monitorsTable~up|monitorGroupsTable~up|monitorgroupsTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up|metricListTable~up|wizardSystemTable~up|stateMachinesTable~up; SortTable=systemTable~2|attributesTable~0|monitorsTable~2|monitorGroupsTable~2|monitorgroupsTable~1|alertsTable~1|locationItemsTable~1|smartLinkTable~0|metricListTable~0|wizardSystemTable~0|stateMachinesTable~1; JSESSIONID="
								+ jSessionId)
				.header("Referer", "http://10.90.1.3:8080/configure.jsp").header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36")
				.asString();
		//System.out.println("SystemFindAgentList" + response.getBody());
	}

	public void findSystemComponentsSetup(SystemObject systemObject) {
		// Unirest.setTimeouts(0, 0);
		System.out.println("Inside SystemPerfmonFindComponentSetup");
		HttpResponse<String> response = Unirest
				.post(systemObject.getOneSightDomain() + "/SystemFindComponentsSetup.htm")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7")
				.header("Accept-Language", "en-US,en;q=0.9").header("Cache-Control", "max-age=0")
				.header("Connection", "keep-alive").header("Content-Type", "application/x-www-form-urlencoded")
				.header("Cookie",
						"SortDirection=systemTable~up|attributesTable~up|monitorsTable~up|monitorGroupsTable~up|monitorgroupsTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up|metricListTable~up|wizardSystemTable~up|stateMachinesTable~up; SortTable=systemTable~2|attributesTable~0|monitorsTable~2|monitorGroupsTable~2|monitorgroupsTable~1|alertsTable~1|locationItemsTable~1|smartLinkTable~0|metricListTable~0|wizardSystemTable~0|stateMachinesTable~1; JSESSIONID="
								+ jSessionId)
				.header("Origin", "http://10.90.1.3:8080")
				.header("Referer", "http://10.90.1.3:8080/SystemFindListAgents.htm")
				.header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36")
				// .field("agentIds", "4").field("_agentIds",
				// "on").field("agentIds", "5").field("_agentIds", "on")
				.field("agentIds", "9").field("_agentIds", "on")
				// .field("agentIds", "19").field("_agentIds", "on")
				// .field("agentIds", "20").field("_agentIds", "on")
				// .field("agentIds", "25").field("_agentIds", "on")
				// .field("agentIds", "36").field("_agentIds",
				// "on").field("agentIds", "41").field("_agentIds", "on")
				.asString();
		//System.out.println("SystemPerfmonFindComponentSetup" + response.getBody());

	}

	public void findJDBCSystemComponentsSetup(SystemObject systemObject) {
		// Unirest.setTimeouts(0, 0);
		System.out.println("Inside SystemJDBCResponse");
		HttpResponse<String> response = Unirest
				.post(systemObject.getOneSightDomain() + "/SystemFindComponentsSetup.htm")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7")
				.header("Accept-Language", "en-US,en;q=0.9").header("Cache-Control", "max-age=0")
				.header("Connection", "keep-alive").header("Content-Type", "application/x-www-form-urlencoded")
				.header("Cookie",
						"SortDirection=systemTable~up|attributesTable~up|monitorsTable~up|monitorGroupsTable~up|monitorgroupsTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up|metricListTable~up|wizardSystemTable~up|stateMachinesTable~up; SortTable=systemTable~2|attributesTable~0|monitorsTable~2|monitorGroupsTable~2|monitorgroupsTable~1|alertsTable~1|locationItemsTable~1|smartLinkTable~0|metricListTable~0|wizardSystemTable~0|stateMachinesTable~1; JSESSIONID="
								+ jSessionId)
				.header("Origin", "http://10.90.1.3:8080")
				.header("Referer", "http://10.90.1.3:8080/SystemFindListAgents.htm")
				.header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36")
				.field("agentIds", "4").field("_agentIds", "on")
				// .field("agentIds", "5").field("_agentIds", "on")
				// .field("agentIds", "9").field("_agentIds",
				// "on").field("agentIds", "19").field("_agentIds", "on")
				// .field("agentIds", "20").field("_agentIds", "on")
				// .field("agentIds", "25").field("_agentIds", "on")
				// .field("agentIds", "36").field("_agentIds",
				// "on").field("agentIds", "41").field("_agentIds", "on")
				.asString();
		//System.out.println("SystemJDBCResponse" + response.getBody());

	}

	public void findSNMPSystemComponentsSetup(SystemObject systemObject) {
		// Unirest.setTimeouts(0, 0);
		HttpResponse<String> response = Unirest
				.post(systemObject.getOneSightDomain() + "/SystemFindComponentsSetup.htm")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7")
				.header("Accept-Language", "en-US,en;q=0.9").header("Cache-Control", "max-age=0")
				.header("Connection", "keep-alive").header("Content-Type", "application/x-www-form-urlencoded")
				.header("Cookie",
						"SortDirection=systemTable~up|attributesTable~up|monitorsTable~up|monitorGroupsTable~up|monitorgroupsTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up|metricListTable~up|wizardSystemTable~up|stateMachinesTable~up; SortTable=systemTable~2|attributesTable~0|monitorsTable~2|monitorGroupsTable~2|monitorgroupsTable~1|alertsTable~1|locationItemsTable~1|smartLinkTable~0|metricListTable~0|wizardSystemTable~0|stateMachinesTable~1; JSESSIONID="
								+ jSessionId)
				.header("Origin", "http://10.90.1.3:8080")
				.header("Referer", "http://10.90.1.3:8080/SystemFindListAgents.htm")
				.header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36")
				// .field("agentIds", "4").field("_agentIds", "on")
				.field("agentIds", "5").field("_agentIds", "on")
				// .field("agentIds", "9").field("_agentIds",
				// "on").field("agentIds", "19").field("_agentIds", "on")
				// .field("agentIds", "20").field("_agentIds", "on")
				// .field("agentIds", "25").field("_agentIds", "on")
				// .field("agentIds", "36").field("_agentIds",
				// "on").field("agentIds", "41").field("_agentIds", "on")
				.asString();
		//System.out.println("SystemFindComponentSetup" + response.getBody());

	}

	public void startSystemDiscovery(SystemObject systemObject) {
		// Unirest.setTimeouts(0, 0);
		HttpResponse<String> response = Unirest.post(systemObject.getOneSightDomain() + "/SystemDiscoveryStart.htm")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7")
				.header("Accept-Language", "en-US,en;q=0.9").header("Cache-Control", "max-age=0")
				.header("Connection", "keep-alive").header("Content-Type", "application/x-www-form-urlencoded")
				.header("Cookie",
						"SortDirection=systemTable~up|attributesTable~up|monitorsTable~up|monitorGroupsTable~up|monitorgroupsTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up|metricListTable~up|wizardSystemTable~up|stateMachinesTable~up; SortTable=systemTable~2|attributesTable~0|monitorsTable~2|monitorGroupsTable~2|monitorgroupsTable~1|alertsTable~1|locationItemsTable~1|smartLinkTable~0|metricListTable~0|wizardSystemTable~0|stateMachinesTable~1; JSESSIONID=A70D6B71039E053546DA7419021818B3")
				.header("Origin", "http://10.90.1.3:8080")
				.header("Referer", "http://10.90.1.3:8080/SystemFindComponentsSetup.htm")
				.header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36")
				/*
				 * .field("discoveryDetails.tabs[1].properties[0].value",
				 * "public")
				 * .field("discoveryDetails.tabs[1].properties[1].value", "15")
				 * .field("discoveryDetails.tabs[1].properties[2].value", "1")
				 * .field("discoveryDetails.tabs[1].properties[3].value", "161")
				 * .field("discoveryDetails.tabs[1].properties[4].value",
				 * "Snmp_Version1")
				 * .field("discoveryDetails.tabs[4].properties[0].value",
				 * "weblogic9.0")
				 * .field("discoveryDetails.tabs[4].properties[1].value", "")
				 * .field("discoveryDetails.tabs[4].properties[2].value",
				 * "7001")
				 * .field("discoveryDetails.tabs[4].properties[3].value",
				 * "false")
				 */
				.field("discoveryDetails.tabs[5].properties[0].value", "Administrator")
				.field("discoveryDetails.tabs[5].properties[1].value", "123Empirix!")
				.field("discoveryDetails.tabs[5].properties[2].value", "").asString();

		//System.out.println("SystemDiscoverySTart" + response.getBody());

	}

	public String systemDiscoveryStatus(SystemObject systemObject) {
		HttpResponse<String> response = Unirest.get(systemObject.getOneSightDomain() + "/SystemDiscoveryStatus.htm")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie",
						"JSESSIONID=" + jSessionId
								+ "; SortDirection=systemTable~up|attributesTable~up|monitorsTable~up|monitorGroupsTable~up|monitorgroupsTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up|metricListTable~up|wizardSystemTable~up|stateMachinesTable~up|appliedMetricTable~up|appliedEntityTable~up; SortTable=systemTable~2|attributesTable~0|monitorsTable~0|monitorGroupsTable~2|monitorgroupsTable~1|alertsTable~1|locationItemsTable~1|smartLinkTable~0|metricListTable~0|wizardSystemTable~0|stateMachinesTable~1|appliedMetricTable~1|appliedEntityTable~1")
				.header("Referer", "http://10.90.1.3:8080/SystemDiscoveryStart.htm")
				.header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36")
				.asString();
		System.out.println("SystemDiscoveryStatus" + response.getBody());
		return response.getBody();

	}

	public void systemFindComponentsRemoved(SystemObject systemObject) {

		HttpResponse<String> response = Unirest
				.get(systemObject.getOneSightDomain() + "/SystemFindComponentsRemoved.htm")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie",
						"SortDirection=systemTable~up|attributesTable~up|monitorsTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up|metricListTable~up|metricTable~up; SortTable=systemTable~2|attributesTable~0|monitorsTable~2|alertsTable~1|locationItemsTable~1|smartLinkTable~0|metricListTable~0|metricTable~1; JSESSIONID="
								+ jSessionId)
				.header("Referer", "http://10.90.1.3:8080/SystemDiscoveryStart.htm")
				.header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36")
				.asString();

	}

	public void systemUpdateTree(SystemObject systemObject) {

		HttpResponse<String> response = Unirest
				.get(systemObject.getOneSightDomain() + "/config/systems/systemUpdateTree.jsp")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie",
						"SortDirection=systemTable~up|attributesTable~up|monitorsTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up|metricListTable~up|metricTable~up; SortTable=systemTable~2|attributesTable~0|monitorsTable~2|alertsTable~1|locationItemsTable~1|smartLinkTable~0|metricListTable~0|metricTable~1; JSESSIONID="
								+ jSessionId)
				.header("Referer", "http://10.90.1.3:8080/SystemModify.htm?systemID=1537")
				.header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36")
				.asString();

	}

	public void findComponents(SystemObject systemObject) {
		HttpResponse<String> response = Unirest.get(systemObject.getOneSightDomain() + "/SystemFindComponents.htm")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie",
						"SortDirection=systemTable~up|attributesTable~up|monitorsTable~up|monitorGroupsTable~up|monitorgroupsTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up|metricListTable~up|wizardSystemTable~up|stateMachinesTable~up; SortTable=systemTable~2|attributesTable~0|monitorsTable~2|monitorGroupsTable~2|monitorgroupsTable~1|alertsTable~1|locationItemsTable~1|smartLinkTable~0|metricListTable~0|wizardSystemTable~0|stateMachinesTable~1; JSESSIONID="
								+ jSessionId)
				.header("Referer", "http://10.90.1.3:8080/SystemDiscoveryStart.htm")
				.header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36")
				.asString();
		//System.out.println("SystemFindComponents" + response.getBody());

	}

	public void systems(SystemObject systemObject) {
		HttpResponse<String> response = Unirest.get(systemObject.getOneSightDomain() + "/Systems.htm")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie",
						"SortDirection=roleTable~up|monitorsTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up|attributesTable~up|monitorGroupsTable~up|monitorgroupsTable~up|schedulesTableByDT~up|monitorsTableByDT~up|appliedMetricTable~up|appliedEntityTable~up|systemTable~up; SortTable=roleTable~1|monitorsTable~0|alertsTable~1|locationItemsTable~1|smartLinkTable~0|attributesTable~0|monitorGroupsTable~2|monitorgroupsTable~1|schedulesTableByDT~0|monitorsTableByDT~0|appliedMetricTable~1|appliedEntityTable~1|systemTable~2; JSESSIONID="
								+ jSessionId)
				.header("Referer", "http://10.90.1.3:8080/configure.jsp").header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();

	}

	public void systemList(SystemObject systemObject) {
		System.out.println("inside systemsList");
		HttpResponse<String> response = Unirest.get(systemObject.getOneSightDomain() + "/SystemsListAPI.htm")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie",
						"JSESSIONID=" + jSessionId
								+ "; SortDirection=roleTable~up|monitorsTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up|attributesTable~up|monitorGroupsTable~up|monitorgroupsTable~up|schedulesTableByDT~up|monitorsTableByDT~up|appliedMetricTable~up|appliedEntityTable~up; SortTable=roleTable~1|monitorsTable~2|alertsTable~1|locationItemsTable~1|smartLinkTable~0|attributesTable~0|monitorGroupsTable~2|monitorgroupsTable~1|schedulesTableByDT~0|monitorsTableByDT~0|appliedMetricTable~1|appliedEntityTable~1")
				.header("Referer", "http://10.90.1.3:8080/Downtime.htm").header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();

		//return response.getBody();

	}

	public void logout(SystemObject systemObject) {

		HttpResponse<String> response = Unirest.get(systemObject.getOneSightDomain() + "/logout.jsp")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie", "JSESSIONID=" + jSessionId)
				.header("Referer", systemObject.getOneSightDomain() + "/Overview.htm")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();

	}

}
