package com.onesightapi.service;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.onesightapi.pojo.DowntimeObject;
import com.onesightapi.utility.MD5;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;

public class GetDowntimeListService {

	String jSessionId = "";
	String blend = "";

	public String process(DowntimeObject downtimeObject) {
		String result = "Error in API";
		try {
			// Unirest.config().followRedirects(false);
			homePage(downtimeObject);
			doLogin(downtimeObject);
			result = downtimeList(downtimeObject);
		}

		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		finally {
			logout(downtimeObject);
		}
		return result;
	}

	public void homePage(DowntimeObject downtimeObject) {

		HttpResponse<String> response = Unirest.get(downtimeObject.getOneSightDomain() + "/login.jsp")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();

		System.out.println(
				"Cookeis ***************************************************************************************************************************************");
		// System.out.println(response.getCookies().get(0).toString().substring("JSESSIONID",";"));

		String pattern1 = "JSESSIONID=";
		String pattern2 = ";";
		String text = response.getCookies().get(0).toString();
		Pattern p = Pattern.compile(Pattern.quote(pattern1) + "(.*?)" + Pattern.quote(pattern2));
		Matcher m = p.matcher(text);
		while (m.find()) {
			System.out.println(m.group(1));
			jSessionId = m.group(1);
		}

		System.out.println(
				"Body ***************************************************************************************************************************************");
		System.out.println(response.getHeaders());

		System.out.println(
				"Body ***************************************************************************************************************************************");
		System.out.println(response.getBody());
		String body = response.getBody();
		int i = body.indexOf("blend += '");
		int j = body.indexOf("document.login.passwordHash.value=hex_md5(blend)");
		blend = body.substring(i, j).replace("';", "").replace("blend += '", "").trim();
	}

	public void doLogin(DowntimeObject downtimeObject) throws UnsupportedEncodingException {

		byte[] decoded = Base64.getDecoder().decode(downtimeObject.getPassword());
		String decodedPassword = new String(decoded, StandardCharsets.UTF_8);

		String passBlend = decodedPassword + blend;
		String passwordHash = new MD5().calcMD5(passBlend);

		HttpResponse<String> response = Unirest
				.get(downtimeObject.getOneSightDomain() + "/do_login.htm?action=login&name="
						+ downtimeObject.getUsername() + "&passwordHashOld="
						+ downtimeObject.getPassword() + "&passwordHash=" + passwordHash)
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie", "JSESSIONID=" + jSessionId).header("Referer", "http://10.90.1.3:8080/login.jsp")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();

		System.out.println(
				"Cookeis ***************************************************************************************************************************************");
		System.out.println(response.getCookies());

		System.out.println(
				"Body ***************************************************************************************************************************************");
		System.out.println(response.getHeaders());

		System.out.println(
				"Body ***************************************************************************************************************************************");
		System.out.println(response.getBody());
	}

	public String downtimeList(DowntimeObject downtimeObject) {
		System.out.println("inside downtime");
		HttpResponse<String> response = Unirest.get(downtimeObject.getOneSightDomain() + "/DowntimesListAPI.htm")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie",
						"JSESSIONID=" + jSessionId + "; SortDirection=roleTable~up|monitorsTable~up|alertsTable~up|locationItemsTable~up|smartLinkTable~up|attributesTable~up|monitorGroupsTable~up|monitorgroupsTable~up|schedulesTableByDT~up|monitorsTableByDT~up|appliedMetricTable~up|appliedEntityTable~up; SortTable=roleTable~1|monitorsTable~2|alertsTable~1|locationItemsTable~1|smartLinkTable~0|attributesTable~0|monitorGroupsTable~2|monitorgroupsTable~1|schedulesTableByDT~0|monitorsTableByDT~0|appliedMetricTable~1|appliedEntityTable~1")
				.header("Referer", "http://10.90.1.3:8080/Downtime.htm").header("Upgrade-Insecure-Requests", "1")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();

		return response.getBody();

	}

	public void logout(DowntimeObject downtimeObject) {

		HttpResponse<String> response = Unirest.get(downtimeObject.getOneSightDomain() + "/logout.jsp")
				.header("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9")
				.header("Accept-Language", "en-US,en;q=0.9").header("Connection", "keep-alive")
				.header("Cookie", "JSESSIONID=" + jSessionId)
				.header("Referer", downtimeObject.getOneSightDomain() + "/Overview.htm")
				.header("User-Agent",
						"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36")
				.asString();

	}

}
