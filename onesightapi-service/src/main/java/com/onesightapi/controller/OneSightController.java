package com.onesightapi.controller;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.onesightapi.pojo.DowntimeObject;
import com.onesightapi.pojo.LocationObject;
import com.onesightapi.pojo.MonitorGroupObject;
import com.onesightapi.pojo.MonitorObject;
import com.onesightapi.pojo.SystemObject;
import com.onesightapi.service.AddMonitorGroupService;
import com.onesightapi.service.AddMonitorService;
import com.onesightapi.service.AddProfileMonitorService;
import com.onesightapi.service.AddSystemService;
import com.onesightapi.service.DeleteMonitorGroupService;
import com.onesightapi.service.DeleteMonitorService;
import com.onesightapi.service.DiscoverSystemService;
import com.onesightapi.service.GetDowntimeListService;
import com.onesightapi.service.GetLocationListService;
import com.onesightapi.service.GetMonitorGroupListService;
import com.onesightapi.service.GetMonitorListService;
import com.onesightapi.service.GetMonitorSampleService;
import com.onesightapi.service.GetSystemListService;

@RestController
public class OneSightController {
	
	@Autowired
	public RestTemplate restTemplate;
	
	@RequestMapping("/")
	public ModelAndView index () {
	    ModelAndView modelAndView = new ModelAndView();
	    modelAndView.setViewName("index.html");
	    return modelAndView;
	}
	
	@RequestMapping(value="/startScheduler", method=RequestMethod.GET)
	public void startScheduler() throws URISyntaxException {
		
		URI uri = new URI("http://discovery-service/start");
		org.springframework.http.HttpHeaders headers = new org.springframework.http.HttpHeaders();
		//headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		//HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

		this.restTemplate.getForObject(uri, String.class);
		
	}

	@RequestMapping(value="/addMonitorGroupAPI", method=RequestMethod.POST)
    public String addMonitorGroup(@RequestBody MonitorGroupObject addMonitorGroupObject) {
        AddMonitorGroupService service = new AddMonitorGroupService();
        return service.process(addMonitorGroupObject);
	 
     }
	
	@RequestMapping(value="/getMonitorGroupListAPI", method=RequestMethod.GET)
    public String getMonitorGroupList(@RequestBody MonitorGroupObject addMonitorGroupObject) {
		GetMonitorGroupListService service = new GetMonitorGroupListService();
        return service.process(addMonitorGroupObject);
	 
     }
	
	@RequestMapping(value="/deleteMonitorGroupAPI", method=RequestMethod.GET)
    public String deleteMonitorGroup(@RequestBody MonitorGroupObject addMonitorGroupObject) {
		DeleteMonitorGroupService service = new DeleteMonitorGroupService();
        return service.process(addMonitorGroupObject);
	 
     }
	
	@RequestMapping(value="/getMonitorListAPI", method=RequestMethod.GET)
    public String getMonitorList(@RequestBody MonitorObject monitorObject) {
		GetMonitorListService service = new GetMonitorListService();
        return service.process(monitorObject);
	 
     }
	
	@RequestMapping(value="/addMonitorAPI", method=RequestMethod.POST)
    public String addMonitor(@RequestBody MonitorObject monitorObject) {
		AddMonitorService service = new AddMonitorService();
        return service.process(monitorObject);
	 
     }
	
	@RequestMapping(value="/getLocationlistAPI", method=RequestMethod.GET)
    public String getLocationList(@RequestBody LocationObject location) {
		GetLocationListService service = new GetLocationListService();
        return service.process(location);
	 
     }
	
	@RequestMapping(value="/deleteMonitorAPI", method=RequestMethod.GET)
    public String deleteMonitor(@RequestBody MonitorObject monitorObject) {
		DeleteMonitorService service = new DeleteMonitorService();
        return service.process(monitorObject);
	 
     }
	
	@RequestMapping(value="/getDowntimeListAPI", method=RequestMethod.GET)
    public String getDowntimelist(@RequestBody DowntimeObject downtime) {
		GetDowntimeListService service = new GetDowntimeListService();
        return service.process(downtime);
	 
     }
	
	@RequestMapping(value="/getSystemListAPI", method=RequestMethod.GET)
    public String getSystemList(@RequestBody SystemObject systemObject) {
    	GetSystemListService service = new GetSystemListService();
        return service.process(systemObject);
	 
     }
	
	@RequestMapping(value="/addSystemAPI", method=RequestMethod.POST)
    public String addSystem(@RequestBody SystemObject systemObject) {
		AddSystemService service = new AddSystemService();
        return service.process(systemObject);
	 
     }
	
	@RequestMapping(value="/discoverSystemAPI", method=RequestMethod.POST)
    public String discoverSystem(@RequestBody SystemObject systemObject) {
		DiscoverSystemService service = new DiscoverSystemService();
        return service.process(systemObject);
     }
	
	@RequestMapping(value="/addProfileMonitorAPI", method=RequestMethod.POST)
    public String addProfileMonitor(@RequestBody MonitorObject monitorObject) {
		AddProfileMonitorService service = new AddProfileMonitorService();
        return service.process(monitorObject);
     }
	
	@RequestMapping(value="/getMonitorSampleValueAPI", method=RequestMethod.GET)
    public String getMonitorSampleValue(@RequestBody MonitorObject monitorObject) {
		GetMonitorSampleService service = new GetMonitorSampleService();
        return service.process(monitorObject);
	 
     }
}
