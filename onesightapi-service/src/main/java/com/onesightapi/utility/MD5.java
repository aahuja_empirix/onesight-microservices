package com.onesightapi.utility;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5 {

    private  MessageDigest algorithm;
    public MD5(){
        try {
            this.algorithm = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

    }
    /*
    * Take a string and return the hex representation of its MD5.
    */

    public String calcMD5(String str) {
        algorithm.reset();
        try {
            algorithm.update(str.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        byte messageDigest[] = algorithm.digest();
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                String hex = Integer.toHexString(0xFF & messageDigest[i]);
                if (hex.length() == 1) {
                    hexString.append('0');
                }
                hexString.append(hex);
            }
            return hexString.toString();

    }

    public static void main(String argv[]) {
       String test="中文";
       System.out.println( new MD5().calcMD5(test) );
    }



}
