package com.onesightapi.pojo;

import java.util.HashMap;

public class SystemDetails {

	private HashMap<String, String> systemTab;
	private HashMap<String, String> snmpTab;
	private HashMap<String, String> snmpTrapTab;
	private HashMap<String, String> virtualAgentTab;
	private HashMap<String, String> jmxTab;
	private HashMap<String, String> windowsNtTab;

	public HashMap<String, String> getSystemTab() {
		return systemTab;
	}

	public void setSystemTab(HashMap<String, String> systemTab) {
		this.systemTab = systemTab;
	}

	public HashMap<String, String> getSnmpTab() {
		return snmpTab;
	}

	public void setSnmpTab(HashMap<String, String> snmpTab) {
		this.snmpTab = snmpTab;
	}

	public HashMap<String, String> getSnmpTrapTab() {
		return snmpTrapTab;
	}

	public void setSnmpTrapTab(HashMap<String, String> snmpTrapTab) {
		this.snmpTrapTab = snmpTrapTab;
	}

	public HashMap<String, String> getVirtualAgentTab() {
		return virtualAgentTab;
	}

	public void setVirtualAgentTab(HashMap<String, String> virtualAgentTab) {
		this.virtualAgentTab = virtualAgentTab;
	}

	public HashMap<String, String> getJmxTab() {
		return jmxTab;
	}

	public void setJmxTab(HashMap<String, String> jmxTab) {
		this.jmxTab = jmxTab;
	}

	public HashMap<String, String> getWindowsNtTab() {
		return windowsNtTab;
	}

	public void setWindowsNtTab(HashMap<String, String> windowsNtTab) {
		this.windowsNtTab = windowsNtTab;
	}

}
