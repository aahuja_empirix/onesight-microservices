package com.onesightapi;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.core.appender.FileAppender;
import org.apache.logging.log4j.core.layout.PatternLayout;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class OneSightAPIService {

	Logger logger = (Logger) LogManager.getLogger(OneSightAPIService.class.getName());

	public static void main(String[] args) {
		SpringApplication.run(OneSightAPIService.class, args);

		final PatternLayout layout = PatternLayout.newBuilder()//
				.withPattern("mylayoutpattern").build();
		final FileAppender appender = FileAppender.newBuilder()//
				.setLayout(layout).withFileName("myFile").withAppend(true).build();
		
	}

	@Bean
	@LoadBalanced
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
}
